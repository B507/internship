﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebApplication1
{
    public partial class CCurrentPosts : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["New"] != null)
            {
                L1.Text = Session["New"].ToString();
            }
            else
                Response.Redirect("CompLogin.aspx");
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            con.Open();
            string Address = "Select Street from Company where Name='" + L1.Text + "'";
            string city = "Select City from Company where Name='" + L1.Text + "'";
            string State = "Select State from Company where Name='" + L1.Text + "'";
            string zip = "Select Zip from Company where Name='" + L1.Text + "'";
            string PhNum = "Select contact_number from Company where Name='" + L1.Text + "'";
            string website = "Select website from Company where Name='" + L1.Text + "'";
            SqlCommand Add = new SqlCommand(Address, con);
            SqlCommand c = new SqlCommand(city, con);
            SqlCommand s = new SqlCommand(State, con);
            SqlCommand z = new SqlCommand(zip, con);
            SqlCommand num = new SqlCommand(PhNum, con);
            SqlCommand web = new SqlCommand(website, con);
            string loc = Add.ExecuteScalar().ToString().Replace(" ", "");
            string city1 = c.ExecuteScalar().ToString().Replace(" ", "");
            string state1 = s.ExecuteScalar().ToString().Replace(" ", "");
            string zip1 = z.ExecuteScalar().ToString().Replace(" ", "");
            string phone = num.ExecuteScalar().ToString().Replace(" ", "");
            string link = web.ExecuteScalar().ToString().Replace(" ", "");
            LocLabel.Text = loc;
            CityLabel.Text = city1;
            StateLabel.Text = state1;
            ZIPLabel.Text = zip1;
            UrlLabel.Text = link;
            NumLabel.Text = phone;

            if (!IsPostBack)
            {
                SqlDataAdapter da = new SqlDataAdapter("select  distinct ip.Internship_Posting_ID, CAST(c.logo AS VARBINARY(MAX)) AS logo, j.Job_ID, j.Job_Title, ip.post_date, c.Name, c.Street, c.city, c.state, c.zip, ip.Desired_Major,ip.Wages,ip.Start_date, ip.End_Date, ip.Hours, j.Job_Description, cp.First_Name from Job j, Internship_Posting ip, Co_Contact_Person cp, Company c , Job_Qualification jq, Job_Qual_Intern jqi where j.Job_ID=ip.Job_ID and cp.Contact_Person_ID=ip.Contact_Person_ID and ip.Internship_Posting_ID=jqi.Internship_Posting_ID and jqi.Job_Qual_ID=jq.Job_Qual_ID and c.CompanyID=cp.CompanyID and c.Name='" + L1.Text + "';", con);
                DataSet ds = new DataSet();
                da.Fill(ds);
                con.Close();
                ListView2.DataSource = ds.Tables[0];
                ListView2.DataBind();
            }
        }
        protected void getView(object sender, EventArgs e)
        {
            foreach (ListViewItem item in ListView2.Items)
            {
                Label mylabel = (Label)item.FindControl("Label7");
                Session["x"] = mylabel.Text;
            }
            Response.Redirect("CView.aspx");
        }

        protected void AddPost_Button_Click(object sender, EventArgs e)
        {
            Response.Redirect("CPostJobs.aspx");
        }

    }
}
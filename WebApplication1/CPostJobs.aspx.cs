﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebApplication1
{
    public partial class Sample : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["New"] != null)
            {
                L1.Text = Session["New"].ToString();
            }
            else
                Response.Redirect("CompLogin.aspx");
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            con.Open();
            string Address = "Select Street from Company where Name='" + L1.Text + "'";
            string city = "Select City from Company where Name='" + L1.Text + "'";
            string State = "Select State from Company where Name='" + L1.Text + "'";
            string zip = "Select Zip from Company where Name='" + L1.Text + "'";
            string PhNum = "Select contact_number from Company where Name='" + L1.Text + "'";
            string website = "Select website from Company where Name='" + L1.Text + "'";
            SqlCommand Add = new SqlCommand(Address, con);
            SqlCommand c = new SqlCommand(city, con);
            SqlCommand s = new SqlCommand(State, con);
            SqlCommand z = new SqlCommand(zip, con);
            SqlCommand num = new SqlCommand(PhNum, con);
            SqlCommand web = new SqlCommand(website, con);
            string loc = Add.ExecuteScalar().ToString().Replace(" ", "");
            string city1 = c.ExecuteScalar().ToString().Replace(" ", "");
            string state1 = s.ExecuteScalar().ToString().Replace(" ", "");
            string zip1 = z.ExecuteScalar().ToString().Replace(" ", "");
            string phone = num.ExecuteScalar().ToString().Replace(" ", "");
            string link = web.ExecuteScalar().ToString().Replace(" ", "");
            LocLabel.Text = loc;
            CityLabel.Text = city1;
            StateLabel.Text = state1;
            ZIPLabel.Text = zip1;
            UrlLabel.Text = link;
            NumLabel.Text = phone;
            
            if (!IsPostBack)
            {
                SqlCommand dd = new SqlCommand("select distinct CourseTitle from course;", con);
                SqlDataAdapter da = new SqlDataAdapter(dd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                DDL_Majors.DataSource = ds;
                DDL_Majors.DataTextField = "CourseTitle";
                DDL_Majors.DataValueField = "CourseTitle";
                DDL_Majors.DataBind();
                DDL_Majors.Items.Insert(0, new ListItem("---Desired Major---", "0"));
            }
            if (!IsPostBack)
            {
                SqlCommand dd2 = new SqlCommand("select First_Name+' '+COALESCE(Last_Name, '') as Name from Co_Contact_Person where CompanyID=(select CompanyID from company where Name='" + L1.Text + "');", con);
                SqlDataAdapter da2 = new SqlDataAdapter(dd2);
                DataSet ds2 = new DataSet();
                da2.Fill(ds2);
                DDL_Contact.DataSource = ds2;
                DDL_Contact.DataTextField = "Name";
                DDL_Contact.DataValueField = "Name";
                DDL_Contact.DataBind();
                DDL_Contact.Items.Insert(0, new ListItem("---Contact Person---", "0"));
            }
            SqlCommand cmd_e = new SqlCommand();
            cmd_e.CommandText = "select Job_Qual_ID, Job_Qual_name from Job_Qualification where req_id=1";
            cmd_e.Connection = con;
            
            using (SqlDataReader sdr = cmd_e.ExecuteReader())
            {
                while (sdr.Read())
                {
                    ListItem item = new ListItem();
                    item.Text = sdr["Job_Qual_name"].ToString();
                    item.Value = sdr["Job_Qual_ID"].ToString();
                    chkOS.Items.Add(item);
                }
            }
            SqlCommand cmd_p = new SqlCommand();
            cmd_p.CommandText = "select Job_Qual_ID, Job_Qual_name from Job_Qualification where req_id=2";
            cmd_p.Connection = con;

            using (SqlDataReader sdr = cmd_p.ExecuteReader())
            {
                while (sdr.Read())
                {
                    ListItem item = new ListItem();
                    item.Text = sdr["Job_Qual_name"].ToString();
                    item.Value = sdr["Job_Qual_ID"].ToString();
                    chklist1.Items.Add(item);
                }
            }
            SqlCommand cmd_o = new SqlCommand();
            cmd_o.CommandText = "select Job_Qual_ID, Job_Qual_name from Job_Qualification where req_id=3";
            cmd_o.Connection = con;

            using (SqlDataReader sdr = cmd_o.ExecuteReader())
            {
                while (sdr.Read())
                {
                    ListItem item = new ListItem();
                    item.Text = sdr["Job_Qual_name"].ToString();
                    item.Value = sdr["Job_Qual_ID"].ToString();
                    chklist2.Items.Add(item);
                }
            }
            SqlCommand cmd_t = new SqlCommand();
            cmd_t.CommandText = "select Job_Qual_ID, Job_Qual_name from Job_Qualification where req_id=4";
            cmd_t.Connection = con;
            
            using (SqlDataReader sdr = cmd_t.ExecuteReader())
            {
                while (sdr.Read())
                {
                    ListItem item = new ListItem();
                    item.Text = sdr["Job_Qual_name"].ToString();
                    item.Value = sdr["Job_Qual_ID"].ToString();
                    chklist3.Items.Add(item);
                }
            }
            

            con.Close();
        }
     /*   protected void DDL_Qual_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            con.Open();
            SqlCommand dd = new SqlCommand("select distinct req_id, skillname from Job_Require where req_id!=" + DDL_Qual.SelectedItem.Value + ";", con);
            SqlDataAdapter da = new SqlDataAdapter(dd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            DDL_Qual0.DataSource = ds;
            DDL_Qual0.DataTextField = "skillname";
            DDL_Qual0.DataValueField = "req_id";
            DDL_Qual0.DataBind();
            DDL_Qual0.Items.Insert(0, new ListItem("---Select---", "0"));

            DDL_Qual0.Visible = true;
            if (DDL_Qual.SelectedValue == "1")
            {

                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "select Job_Qual_ID, Job_Qual_name from Job_Qualification where req_id=1";
                        cmd.Connection = conn;
                        conn.Open();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            while (sdr.Read())
                            {
                                ListItem item = new ListItem();
                                item.Text = sdr["Job_Qual_name"].ToString();
                                item.Value = sdr["Job_Qual_ID"].ToString();
                                chkOS.Items.Add(item);
                            }
                        }
                        con.Close();
                    }
                }

            }
            else if (DDL_Qual.SelectedValue == "2")
            {
                //this.list2();
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "select Job_Qual_ID, Job_Qual_name from Job_Qualification where req_id=2";
                        cmd.Connection = conn;
                        conn.Open();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            while (sdr.Read())
                            {
                                ListItem item = new ListItem();
                                item.Text = sdr["Job_Qual_name"].ToString();
                                item.Value = sdr["Job_Qual_ID"].ToString();
                                chkOS.Items.Add(item);
                            }
                        }
                        conn.Close();
                    }
                }
            }
            else if (DDL_Qual.SelectedValue == "3")
            {
                //this.list3();
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "select Job_Qual_ID, Job_Qual_name from Job_Qualification where req_id=3";
                        cmd.Connection = conn;
                        conn.Open();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            while (sdr.Read())
                            {
                                ListItem item = new ListItem();
                                item.Text = sdr["Job_Qual_name"].ToString();
                                item.Value = sdr["Job_Qual_ID"].ToString();
                                chkOS.Items.Add(item);
                            }
                        }
                        con.Close();
                    }
                }
            }
            else if (DDL_Qual.SelectedValue == "4")
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "select Job_Qual_ID, Job_Qual_name from Job_Qualification where req_id=4";
                        cmd.Connection = conn;
                        conn.Open();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            while (sdr.Read())
                            {
                                ListItem item = new ListItem();
                                item.Text = sdr["Job_Qual_name"].ToString();
                                item.Value = sdr["Job_Qual_ID"].ToString();
                                chkOS.Items.Add(item);
                            }
                        }
                        con.Close();
                    }
                }
            }

            else
            {

            }

        }

        protected void DDL_Qual0_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            con.Open();
            SqlCommand dd = new SqlCommand("select distinct req_id, skillname from Job_Require where req_id!=" + DDL_Qual.SelectedItem.Value + "and req_id!=" + DDL_Qual0.SelectedItem.Value + ";", con);
            SqlDataAdapter da = new SqlDataAdapter(dd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            DDL_Qual1.DataSource = ds;
            DDL_Qual1.DataTextField = "skillname";
            DDL_Qual1.DataValueField = "req_id";
            DDL_Qual1.DataBind();
            DDL_Qual1.Items.Insert(0, new ListItem("---Select---", "0"));

            DDL_Qual1.Visible = true;
            if (DDL_Qual0.SelectedValue == "1")
            {

                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "select Job_Qual_ID, Job_Qual_name from Job_Qualification where req_id=1";
                        cmd.Connection = conn;
                        conn.Open();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            while (sdr.Read())
                            {
                                ListItem item = new ListItem();
                                item.Text = sdr["Job_Qual_name"].ToString();
                                item.Value = sdr["Job_Qual_ID"].ToString();
                                chklist1.Items.Add(item);
                            }
                        }
                        con.Close();
                    }
                }

            }
            else if (DDL_Qual0.SelectedValue == "2")
            {

                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "select Job_Qual_ID, Job_Qual_name from Job_Qualification where req_id=2";
                        cmd.Connection = conn;
                        conn.Open();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            while (sdr.Read())
                            {
                                ListItem item = new ListItem();
                                item.Text = sdr["Job_Qual_name"].ToString();
                                item.Value = sdr["Job_Qual_ID"].ToString();
                                chklist1.Items.Add(item);
                            }
                        }
                        conn.Close();
                    }
                }
            }
            else if (DDL_Qual0.SelectedValue == "3")
            {

                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "select Job_Qual_ID, Job_Qual_name from Job_Qualification where req_id=3";
                        cmd.Connection = conn;
                        conn.Open();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            while (sdr.Read())
                            {
                                ListItem item = new ListItem();
                                item.Text = sdr["Job_Qual_name"].ToString();
                                item.Value = sdr["Job_Qual_ID"].ToString();
                                chklist1.Items.Add(item);
                            }
                        }
                        con.Close();
                    }
                }
            }
            else if (DDL_Qual0.SelectedValue == "4")
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "select Job_Qual_ID, Job_Qual_name from Job_Qualification where req_id=4";
                        cmd.Connection = conn;
                        conn.Open();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            while (sdr.Read())
                            {
                                ListItem item = new ListItem();
                                item.Text = sdr["Job_Qual_name"].ToString();
                                item.Value = sdr["Job_Qual_ID"].ToString();
                                chklist1.Items.Add(item);
                            }
                        }
                        con.Close();
                    }
                }
            }

            else
            {

            }

        }

        protected void DDL_Qual1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            con.Open();
            SqlCommand dd = new SqlCommand("select distinct req_id, skillname from Job_Require where req_id!=" + DDL_Qual.SelectedItem.Value + "and req_id!=" + DDL_Qual0.SelectedItem.Value + "and req_id!=" + DDL_Qual1.SelectedItem.Value + ";", con);
            SqlDataAdapter da = new SqlDataAdapter(dd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            DDL_Qual2.DataSource = ds;
            DDL_Qual2.DataTextField = "skillname";
            DDL_Qual2.DataValueField = "req_id";
            DDL_Qual2.DataBind();
            DDL_Qual2.Items.Insert(0, new ListItem("---Select---", "0"));

            DDL_Qual2.Visible = true;
            if (DDL_Qual1.SelectedValue == "1")
            {

                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "select Job_Qual_ID, Job_Qual_name from Job_Qualification where req_id=1";
                        cmd.Connection = conn;
                        conn.Open();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            while (sdr.Read())
                            {
                                ListItem item = new ListItem();
                                item.Text = sdr["Job_Qual_name"].ToString();
                                item.Value = sdr["Job_Qual_ID"].ToString();
                                chklist2.Items.Add(item);
                            }
                        }
                        con.Close();
                    }
                }

            }
            else if (DDL_Qual1.SelectedValue == "2")
            {

                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "select Job_Qual_ID, Job_Qual_name from Job_Qualification where req_id=2";
                        cmd.Connection = conn;
                        conn.Open();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            while (sdr.Read())
                            {
                                ListItem item = new ListItem();
                                item.Text = sdr["Job_Qual_name"].ToString();
                                item.Value = sdr["Job_Qual_ID"].ToString();
                                chklist2.Items.Add(item);
                            }
                        }
                        conn.Close();
                    }
                }
            }
            else if (DDL_Qual1.SelectedValue == "3")
            {

                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "select Job_Qual_ID, Job_Qual_name from Job_Qualification where req_id=3";
                        cmd.Connection = conn;
                        conn.Open();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            while (sdr.Read())
                            {
                                ListItem item = new ListItem();
                                item.Text = sdr["Job_Qual_name"].ToString();
                                item.Value = sdr["Job_Qual_ID"].ToString();
                                chklist2.Items.Add(item);
                            }
                        }
                        con.Close();
                    }
                }
            }
            else if (DDL_Qual1.SelectedValue == "4")
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "select Job_Qual_ID, Job_Qual_name from Job_Qualification where req_id=4";
                        cmd.Connection = conn;
                        conn.Open();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            while (sdr.Read())
                            {
                                ListItem item = new ListItem();
                                item.Text = sdr["Job_Qual_name"].ToString();
                                item.Value = sdr["Job_Qual_ID"].ToString();
                                chklist2.Items.Add(item);
                            }
                        }
                        con.Close();
                    }
                }
            }

            else
            {

            }

        }

        protected void DDL_Qual2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DDL_Qual2.SelectedValue == "1")
            {

                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "select Job_Qual_ID, Job_Qual_name from Job_Qualification where req_id=1";
                        cmd.Connection = conn;
                        conn.Open();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            while (sdr.Read())
                            {
                                ListItem item = new ListItem();
                                item.Text = sdr["Job_Qual_name"].ToString();
                                item.Value = sdr["Job_Qual_ID"].ToString();
                                chklist3.Items.Add(item);
                            }
                        }
                        conn.Close();
                    }
                }

            }
            else if (DDL_Qual2.SelectedValue == "2")
            {

                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "select Job_Qual_ID, Job_Qual_name from Job_Qualification where req_id=2";
                        cmd.Connection = conn;
                        conn.Open();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            while (sdr.Read())
                            {
                                ListItem item = new ListItem();
                                item.Text = sdr["Job_Qual_name"].ToString();
                                item.Value = sdr["Job_Qual_ID"].ToString();
                                chklist3.Items.Add(item);
                            }
                        }
                        conn.Close();
                    }
                }
            }
            else if (DDL_Qual2.SelectedValue == "3")
            {

                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "select Job_Qual_ID, Job_Qual_name from Job_Qualification where req_id=3";
                        cmd.Connection = conn;
                        conn.Open();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            while (sdr.Read())
                            {
                                ListItem item = new ListItem();
                                item.Text = sdr["Job_Qual_name"].ToString();
                                item.Value = sdr["Job_Qual_ID"].ToString();
                                chklist3.Items.Add(item);
                            }
                        }
                        conn.Close();
                    }
                }
            }
            else if (DDL_Qual2.SelectedValue == "4")
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "select Job_Qual_ID, Job_Qual_name from Job_Qualification where req_id=4";
                        cmd.Connection = conn;
                        conn.Open();
                        using (SqlDataReader sdr = cmd.ExecuteReader())
                        {
                            while (sdr.Read())
                            {
                                ListItem item = new ListItem();
                                item.Text = sdr["Job_Qual_name"].ToString();
                                item.Value = sdr["Job_Qual_ID"].ToString();
                                chklist3.Items.Add(item);
                            }
                        }
                        conn.Close();
                    }
                }
            }

            else
            {

            }


        }*/
        
        protected void Create_Button_Click(object sender, EventArgs e)
        {
            SqlConnection vid = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            {
                try
                {
                    vid.Open();
                    SqlCommand com2 = new SqlCommand("select Contact_Person_ID from Co_Contact_Person where First_Name+' '+COALESCE(Last_Name, '')=LTRIM(RTRIM('" + DDL_Contact.SelectedValue + "'))", vid);
                    int cp_id = Convert.ToInt32(com2.ExecuteScalar());
                    SqlCommand com = new SqlCommand("Insert into Job  values(@job_title, @job_description)", vid);
                    com.Parameters.AddWithValue("@job_title", Job_Title.Text);
                    com.Parameters.AddWithValue("@job_description", Job_Description.Value);
                    com.ExecuteNonQuery();
                    SqlCommand com_job = new SqlCommand("select max(Job_ID) from Job", vid);
                    int j_id = Convert.ToInt32(com_job.ExecuteScalar());
                    SqlCommand d = new SqlCommand("select getdate()", vid);
                    DateTime dt = Convert.ToDateTime(d.ExecuteScalar());
                    SqlCommand com1 = new SqlCommand("Insert into Internship_Posting (Contact_Person_ID, Job_ID, Wages, Start_Date, End_Date, Hours, Desired_Major, post_date) values(" + cp_id + ", " + j_id + ", @wages, @start_date, @end_date, @hours, @desired_major, '" + dt + "')", vid);

                    com1.Parameters.AddWithValue("@desired_major", DDL_Majors.Text);
                    com1.Parameters.AddWithValue("@hours", Hours.Text);
                    com1.Parameters.AddWithValue("@end_date", End_Date.Text);
                    com1.Parameters.AddWithValue("@start_date", Start_Date.Text);
                    com1.Parameters.AddWithValue("@wages", Salary.Text);
                    com1.ExecuteNonQuery();


                    //SqlCommand jq = new SqlCommand("select Job_Qual_ID from Job_Qualification where Job_Qual_Name=LTRIM(RTRIM('" + DDL_Qual0.Text + "'))", vid);
                    //int q_id = Convert.ToInt32(jq.ExecuteScalar());
                    SqlCommand ip = new SqlCommand("select max(Internship_Posting_ID) from Internship_Posting", vid);
                    int ip_id = Convert.ToInt32(ip.ExecuteScalar());
                    /*SqlCommand jqi = new SqlCommand("Insert into Job_Qual_Intern values(" + q_id + ", " + ip_id + ")", vid);
                    jqi.ExecuteNonQuery();*/
                    using (SqlConnection conn = new SqlConnection())
                    {
                        conn.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.CommandText = "Insert into Job_Qual_Intern values(@Job_Qual_ID, " + ip_id + ")";
                            cmd.Connection = conn;
                            conn.Open();
                            foreach (ListItem item in chkOS.Items)
                            {
                                if (item.Selected)
                                {
                                    cmd.Parameters.Clear();
                                    cmd.Parameters.AddWithValue("@Job_Qual_ID", item.Value);
                                    cmd.ExecuteNonQuery();
                                }
                            }
                            foreach (ListItem item in chklist1.Items)
                            {
                                if (item.Selected)
                                {
                                    cmd.Parameters.Clear();
                                    cmd.Parameters.AddWithValue("@Job_Qual_ID", item.Value);
                                    cmd.ExecuteNonQuery();
                                }
                            }
                            foreach (ListItem item in chklist2.Items)
                            {
                                if (item.Selected)
                                {
                                    cmd.Parameters.Clear();
                                    cmd.Parameters.AddWithValue("@Job_Qual_ID", item.Value);
                                    cmd.ExecuteNonQuery();
                                }
                            }
                            foreach (ListItem item in chklist3.Items)
                            {
                                if (item.Selected)
                                {
                                    cmd.Parameters.Clear();
                                    cmd.Parameters.AddWithValue("@Job_Qual_ID", item.Value);
                                    cmd.ExecuteNonQuery();
                                }
                            }
                            conn.Close();
                        }
                    }
                    vid.Close();


                    //ResultLable.Text = Convert.ToString(q_id);
                    ResultLable.Visible = true;
                }
                catch (Exception ex)
                {
                    Response.Write("Error:" + ex.ToString());
                }
            }
        }
    }
}
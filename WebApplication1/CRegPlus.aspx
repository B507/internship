﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CRegPlus.aspx.cs" Inherits="WebApplication1.CRegPlus" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="sidebar.css">
  <link rel="stylesheet" href="datepicker.css">
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="datepicker.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper" class="container-fluid ">

 <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#"><h3><b>Company Portal<b></h3></a>
                </li>
				
                
                
			<img src="mnsulogo.jpg" alt="mnsulogo" class="fix3" ></img>
			<li class="fix4">
                    <a href="CompLogin.aspx"> Go to LogIn page</a>
                </li>
        </div>
<div id = "form-container" class="container-fluid left">
<a href="#justify-icon" class="glyphicon glyphicon-align-justify size" id='justify-icon'></a>
</div>


<div class="fix2 container-fluid">

<div class="container-fluid left"  >
<h1 class="headclr">Company Registration</h1>
</div>
<div id="rcorners4">
<div class="container-fluid ">

<div class="two left">

</div>
<div class="user one ">
</div>
</div>
</div>

</div>



<br/>

 <br/>

  <div id="rcorners3"  class="container-fluid form-inline center2 ">
  <table>
            <tr>
                <td>
                    <asp:Label ID="Step1Lable" runat="server" Text="Step - 1: Company Information" Font-Bold="True"></asp:Label>
                </td>
                <td>
                    </td>
                <td>
                    </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="CompanyNameTB" runat="server" Width="315px" class="form-control" Enabled="false" placeholder="Company Name"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="PhNumTB" runat="server" class="form-control" Width="300px" TextMode="Number" Enabled="false" placeholder="Phone Number"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="MailTB" runat="server" class="form-control" Width="315px" TextMode="Email" Enabled="false" placeholder="Email Address"></asp:TextBox>
                    </td>
                <td>
                    <asp:TextBox ID="LinkTB" runat="server" class="form-control" Width="300px" Enabled="false" placeholder="Company Website"></asp:TextBox>
                </td>
                <td>
                   </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="CompanyStreetTB" runat="server" class="form-control" Width="90px" Enabled="false" placeholder="Street"></asp:TextBox>
                    <asp:TextBox ID="CompanyCityTB" runat="server" class="form-control" Width="85px" Enabled="false" placeholder="City"></asp:TextBox>
                    <asp:TextBox ID="CompanyStateTB" runat="server" class="form-control" Width="50px" Enabled="false" placeholder="State"></asp:TextBox>
                    <asp:TextBox ID="CompanyZIPTB" runat="server" class="form-control" Width="68px" Enabled="false" placeholder="ZIP"></asp:TextBox>
                    </td>
                <td>
                    <br />
                </td>
                <td>
                   </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="UploadLable" runat="server" Enabled="false" Text="Upload Logo"></asp:Label>
                </td>
                <td>
                    <asp:FileUpload ID="FileUpload1" Enabled="false" class="form-control" runat="server" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" style="text-align: center" Text="Step - 2: Contact Person Details" Font-Bold="True"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="ConPersonNameTB" class="form-control" runat="server" Width="315px" placeholder="First Name, Last Name"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="JobTitleTB" class="form-control" runat="server" Width="150px" placeholder="Job Title"></asp:TextBox>
                    <asp:TextBox ID="DeptTB" class="form-control" runat="server" Width="150px" placeholder="Department"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="CMailTB" class="form-control" runat="server" TextMode="Email" Width="315px" placeholder="Email Address"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="CPhNumTB" class="form-control" runat="server" TextMode="Number" Width="305px" placeholder="Phone Number"></asp:TextBox>
                </td>
                <td>
                    </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="ContactAddressTB" class="form-control" runat="server" Width="100px" placeholder="Company Address"></asp:TextBox>
                    <asp:TextBox ID="ContactStateTB" class="form-control" runat="server" Width="60px" placeholder="State"></asp:TextBox>
                    <asp:TextBox ID="ContactZIPTB" class="form-control" runat="server" Width="77px" placeholder="ZIP"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
                <td >
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="UserNameLable"  runat="server" Text="User Name"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="UserNameTB" Enabled="false" class="form-control" runat="server" Width="258px" ViewStateMode="Enabled"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="UserNameTB" EnableTheming="True" ErrorMessage="UserName is required" ForeColor="#FF3300" ValidationGroup="val2"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="PasswordLable" runat="server" Text="Password"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="PasswordTB" Enabled="false" class="form-control" runat="server" Width="258px" TextMode="Password"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Enabled="false" ControlToValidate="PasswordTB" ErrorMessage="Password is required" ForeColor="#FF3300" ValidationGroup="val2"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="ConPasswordLable" runat="server" Text="Confirm Password"></asp:Label>
                &nbsp;
                </td>
                <td>
                    <asp:TextBox ID="ConPasswordTB" class="form-control" runat="server" Enabled="false" Width="258px" TextMode="Password"></asp:TextBox>
                    <br />
                </td>
                <td>
                    
                </td>
            </tr>
            <tr>
                <td>
              
                    <asp:Label ID="Label2" runat="server" Visible="False" Text="Registration is successful" ForeColor="#009933" ValidationGroup="val2"></asp:Label>
                </td>
                <td>             
                    &nbsp;
                        <asp:Button ID="RegisterButton" runat="server" OnClick="RegisterButton_Click" Text="Register" class="btn purple" ValidationGroup="val2" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="plusbutton" runat="server" Text=" + " class="btn purple" OnClick="plusbutton_Click" ValidationGroup="val2"/>
                    </td>
                <td>                
                    <input id="Reset1Button" type="reset" value="Reset" class="btn purple" /></td>
            </tr>
            <tr>
                <td 
                   <asp:LinkButton href="CompLogin.aspx" ID="ContLoginLinkButton" runat="server" target="_top" Visible="False">Continue with Login</asp:LinkButton>
                </td>
                <td 
              
                    <asp:Label ID="Label3" runat="server" Visible="False" Text="User already exists" ForeColor="#FF3300" style="text-align: center" ValidationGroup="val2"></asp:Label>
                </td>
                <td ></td>
            </tr>
            </table>
</div>
 <br/>
  
  


	
   
    </div>
  

 </form>
</body>
<script>
$(function () {
    $("#btnAdd").click( function () {
    var cont = $("#add-form").clone();
	$(cont).find("#btnAdd").replaceWith('<input id="btnremove" type="button" value="-" class="btn btn-circle btn-danger remove" />').end().appendTo("#form-container");
	});
	 
    $("#form-container").on("click", ".remove", function () {
     $(this).closest("div.form1").remove();

    });
	 $("#justify-icon").click(function() {
      
        $("#wrapper").toggleClass("toggled");
    });
	
});

</script>

</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;

namespace WebApplication1
{
    public partial class CRegPlus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["x"] != null)
            {
                UserNameTB.Text = Session["x"].ToString();
            }
            else
            {
                UserNameTB.Text = "not provided";
            }
        }
        protected void RegisterButton_Click(object sender, EventArgs e)
        {
            SqlConnection vod = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            vod.Open();
            SqlCommand com2 = new SqlCommand("Insert into Co_Contact_Person(First_Name, CompanyID, job_title, mailID, contact_number, Department, Comp_Address, State, ZIP) values(@First_Name, @CompanyID, @job_title, @mailID, @contact_number, @Department, @Comp_Address, @State, @ZIP)", vod);
            com2.Parameters.AddWithValue("@First_Name", ConPersonNameTB.Text);
            com2.Parameters.AddWithValue("@CompanyID", UserNameTB.Text);
            com2.Parameters.AddWithValue("@job_title", JobTitleTB.Text);
            com2.Parameters.AddWithValue("@mailID", MailTB.Text);
            com2.Parameters.AddWithValue("@Contact_number", PhNumTB.Text);
            com2.Parameters.AddWithValue("@Department", DeptTB.Text);
            com2.Parameters.AddWithValue("@Comp_Address", ContactAddressTB.Text);
            com2.Parameters.AddWithValue("@State", ContactStateTB.Text);
            com2.Parameters.AddWithValue("@ZIP", ContactZIPTB.Text);
            try
            {
                com2.ExecuteNonQuery();
                Label2.Visible = true;
            }
            catch (Exception ex)
            {
                Response.Write("Error:" + ex.ToString());
            }
            vod.Close();
        }

        protected void plusbutton_Click(object sender, EventArgs e)
        {
            SqlConnection vod = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            vod.Open();
            SqlCommand com2 = new SqlCommand("Insert into Co_Contact_Person(First_Name, CompanyID, job_title, mailID, contact_number, Department, Comp_Address, State, ZIP) values(@First_Name, @CompanyID, @job_title, @mailID, @contact_number, @Department, @Comp_Address, @State, @ZIP)", vod);
            com2.Parameters.AddWithValue("@First_Name", ConPersonNameTB.Text);
            com2.Parameters.AddWithValue("@CompanyID", UserNameTB.Text);
            com2.Parameters.AddWithValue("@job_title", JobTitleTB.Text);
            com2.Parameters.AddWithValue("@mailID", MailTB.Text);
            com2.Parameters.AddWithValue("@Contact_number", PhNumTB.Text);
            com2.Parameters.AddWithValue("@Department", DeptTB.Text);
            com2.Parameters.AddWithValue("@Comp_Address", ContactAddressTB.Text);
            com2.Parameters.AddWithValue("@State", ContactStateTB.Text);
            com2.Parameters.AddWithValue("@ZIP", ContactZIPTB.Text);
            try
            {
                com2.ExecuteNonQuery();
                Response.Redirect("CompRegisterPlus.aspx");
            }
            catch (Exception ex)
            {
                Response.Write("Error:" + ex.ToString());
            }
            vod.Close();
        }
    }
}
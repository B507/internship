﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;

namespace WebApplication1
{
    public partial class CSetup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void RegisterButton_Click(object sender, EventArgs e)
        {
            try
            {

                SqlConnection vod = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                vod.Open();
                string checkuser = "select count(*) from login where Username= LTRIM(RTRIM(' " + UserNameTB.Text + " '))";
                SqlCommand comm = new SqlCommand(checkuser, vod);
                int temp = Convert.ToInt32(comm.ExecuteScalar().ToString());

                if (temp == 1)
                {
                    Label3.Visible = true;
                }
                else
                {
                    SqlCommand com = new SqlCommand("Insert into Company(CompanyID, Name, contact_number, website, Street, City, State, ZIP) values(@CompanyID, @Name, @contact_number, @website, @Street, @City, @State, @ZIP)", vod);
                    SqlCommand con = new SqlCommand("insert into login values (@uname, @pwd, @Name)", vod);
                    SqlCommand com2 = new SqlCommand("Insert into Co_Contact_Person(First_Name, CompanyID, job_title, mailID, contact_number, Department, Comp_Address, State, ZIP) values(@First_Name, @CompanyID, @job_title, @mailID, @contact_number, @Department, @Comp_Address, @State, @ZIP)", vod);

                    com.Parameters.AddWithValue("@CompanyID", UserNameTB.Text);
                    com.Parameters.AddWithValue("@Name", CompanyNameTB.Text);
                    com.Parameters.AddWithValue("@Street", CompanyStreetTB.Text);
                    com.Parameters.AddWithValue("@City", CompanyCityTB.Text);
                    com.Parameters.AddWithValue("@State", CompanyStateTB.Text);
                    com.Parameters.AddWithValue("@ZIP", CompanyZIPTB.Text);
                    com.Parameters.AddWithValue("@contact_number", PhNumTB.Text);
                    com.Parameters.AddWithValue("@website", LinkTB.Text);

                    con.Parameters.AddWithValue("@uname", UserNameTB.Text);
                    con.Parameters.AddWithValue("@pwd", PasswordTB.Text);
                    con.Parameters.AddWithValue("@Name", CompanyNameTB.Text);

                    com2.Parameters.AddWithValue("@First_Name", ConPersonNameTB.Text);
                    com2.Parameters.AddWithValue("@CompanyID", UserNameTB.Text);
                    com2.Parameters.AddWithValue("@job_title", JobTitleTB.Text);
                    com2.Parameters.AddWithValue("@mailID", MailTB.Text);
                    com2.Parameters.AddWithValue("@Contact_number", PhNumTB.Text);
                    com2.Parameters.AddWithValue("@Department", DeptTB.Text);
                    com2.Parameters.AddWithValue("@Comp_Address", ContactAddressTB.Text);
                    com2.Parameters.AddWithValue("@State", ContactStateTB.Text);
                    com2.Parameters.AddWithValue("@ZIP", ContactZIPTB.Text);


                    try
                    {
                        com.ExecuteNonQuery();
                        con.ExecuteNonQuery();
                        com2.ExecuteNonQuery();

                        if (FileUpload1.HasFile)
                        {
                            FileUpload1.SaveAs(Server.MapPath("~/uploads/" + FileUpload1.FileName));
                            string path = "~/uploads/" + FileUpload1.FileName.ToString();
                            SqlCommand i = new SqlCommand("SELECT IDENT_CURRENT ('Co_Contact_Person')", vod);
                            SqlCommand com3 = new SqlCommand("update company set logo='" + path + "' where companyID='" + UserNameTB.Text + "';", vod);
                            com3.ExecuteNonQuery();
                            Label2.Visible = true;
                            ContLoginLinkButton.Visible = true;
                            Session["x"] = UserNameTB.Text;
                            vod.Close();
                        }
                        else
                        {
                            Label3.Text = "Please upload Logo";
                            Label3.Visible = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        Response.Write("Error:" + ex.ToString());
                    }
                }
                vod.Close();
            }
            catch (Exception ex)
            {
                Response.Write("Error:" + ex.ToString());
            }

        }

        protected void plusbutton_Click(object sender, EventArgs e)
        {
            try
            {

                SqlConnection vod = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                vod.Open();
                string checkuser = "select count(*) from login where Username= LTRIM(RTRIM(' " + UserNameTB.Text + " '))";
                SqlCommand comm = new SqlCommand(checkuser, vod);
                int temp = Convert.ToInt32(comm.ExecuteScalar().ToString());

                if (temp == 1)
                {
                    Label3.Visible = true;
                }
                else
                {
                    SqlCommand com = new SqlCommand("Insert into Company(CompanyID, Name, contact_number, website, Street, City, State, ZIP) values(@CompanyID, @Name, @contact_number, @website, @Street, @City, @State, @ZIP)", vod);
                    SqlCommand con = new SqlCommand("insert into login values (@uname, @pwd, @Name)", vod);
                    SqlCommand com2 = new SqlCommand("Insert into Co_Contact_Person(First_Name, CompanyID, job_title, mailID, contact_number, Department, Comp_Address, State, ZIP) values(@First_Name, @CompanyID, @job_title, @mailID, @contact_number, @Department, @Comp_Address, @State, @ZIP)", vod);

                    com.Parameters.AddWithValue("@CompanyID", UserNameTB.Text);
                    com.Parameters.AddWithValue("@Name", CompanyNameTB.Text);
                    com.Parameters.AddWithValue("@Street", CompanyStreetTB.Text);
                    com.Parameters.AddWithValue("@City", CompanyCityTB.Text);
                    com.Parameters.AddWithValue("@State", CompanyStateTB.Text);
                    com.Parameters.AddWithValue("@ZIP", CompanyZIPTB.Text);
                    com.Parameters.AddWithValue("@contact_number", PhNumTB.Text);
                    com.Parameters.AddWithValue("@website", LinkTB.Text);

                    con.Parameters.AddWithValue("@uname", UserNameTB.Text);
                    con.Parameters.AddWithValue("@pwd", PasswordTB.Text);
                    con.Parameters.AddWithValue("@Name", CompanyNameTB.Text);

                    com2.Parameters.AddWithValue("@First_Name", ConPersonNameTB.Text);
                    com2.Parameters.AddWithValue("@CompanyID", UserNameTB.Text);
                    com2.Parameters.AddWithValue("@job_title", JobTitleTB.Text);
                    com2.Parameters.AddWithValue("@mailID", MailTB.Text);
                    com2.Parameters.AddWithValue("@Contact_number", PhNumTB.Text);
                    com2.Parameters.AddWithValue("@Department", DeptTB.Text);
                    com2.Parameters.AddWithValue("@Comp_Address", ContactAddressTB.Text);
                    com2.Parameters.AddWithValue("@State", ContactStateTB.Text);
                    com2.Parameters.AddWithValue("@ZIP", ContactZIPTB.Text);


                    try
                    {
                        com.ExecuteNonQuery();
                        con.ExecuteNonQuery();
                        com2.ExecuteNonQuery();

                        if (FileUpload1.HasFile)
                        {
                            FileUpload1.SaveAs(Server.MapPath("~/uploads/" + FileUpload1.FileName));
                            string path = "~/uploads/" + FileUpload1.FileName.ToString();
                            SqlCommand i = new SqlCommand("SELECT IDENT_CURRENT ('Co_Contact_Person')", vod);
                            SqlCommand com3 = new SqlCommand("update company set logo='" + path + "' where companyID='" + UserNameTB.Text + "';", vod);
                            com3.ExecuteNonQuery();
                            Session["x"] = UserNameTB.Text;
                            Response.Redirect("CRegPlus.aspx");
                        }
                        else
                        {
                            Label3.Text = "Please upload Logo";
                            Label3.Visible = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        Response.Write("Error:" + ex.ToString());
                    }
                }
                vod.Close();
            }
            catch (Exception ex)
            {
                Response.Write("Error:" + ex.ToString());
            }

        }
    }
}
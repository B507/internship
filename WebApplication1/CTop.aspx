﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CTop.aspx.cs" Inherits="WebApplication1.CTop" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="sidebar.css">
  <link rel="stylesheet" href="datepicker.css">
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="datepicker.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper" class="container-fluid ">

 <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#CTop.aspx"><h3><b>Company Portal<b></h3></a>
                </li>
				
                <li>
                    <a href="CPostJobs.aspx">Post Jobs</a>
                </li>
				
                <li>
                    <a href="CCurrentPosts.aspx">Current Postings</a>
                </li>
				
                <li>
                    <a href="https://docs.google.com/forms/d/1imbwezd520Qo7CHJ5SIYAfRCliFiHdp_9hIgfNhXMgU/viewform">Post Feedback</a>
                </li>
				
                
			<img src="mnsulogo.jpg" alt="mnsulogo" class="fix3" ></img>
			<li class="fix4">
                    <a href="CompLogin.aspx">Logout</a>
                </li>
        </div>
<div id = "form-container" class="container-fluid left">
<a href="#justify-icon" class="glyphicon glyphicon-align-justify size" id='justify-icon'></a>
</div>


<div class="fix2 container-fluid">

<div class="container-fluid left"  >
<h1 class="headclr">Company Portal </h1>
</div>
<div id="rcorners4">
<div class="container-fluid ">

<div class="two left">
<p><asp:Label ID="L1" runat="server" Text="Label" Font-Bold="True"></asp:Label></p>
<p><asp:Label ID="LocLabel" runat="server" Text="Label" Font-Size="X-Small"></asp:Label>
                               ,
                   <asp:Label ID="CityLabel" runat="server" Text="Label" Font-Size="X-Small"></asp:Label>
                               ,
                   <asp:Label ID="StateLabel" runat="server" Text="Label" Font-Size="X-Small"></asp:Label>
                               ,
                   <asp:Label ID="ZIPLabel" runat="server" Text="Label" Font-Size="X-Small"></asp:Label></p>
<p style="font-size:x-small">Ph Number:<asp:Label ID="NumLabel" runat="server" Text="Label" Font-Size="X-Small"></asp:Label></p>
<p style="font-size:x-small">Website:<asp:Label ID="UrlLabel" runat="server" Text="Label" Font-Size="X-Small"></asp:Label></p>
</div>

</div>
</div>

</div>



<br/>
    <div class="form-inline container-fluid center1 " >

    


 
 </div>
 <br/>
  <div id="rcorners3"  class="container-fluid form-inline center2 ">
      
</div>
 
</form>
</body>
    <script>
$(function () {
    $("#btnAdd").click( function () {
    var cont = $("#add-form").clone();
	$(cont).find("#btnAdd").replaceWith('<input id="btnremove" type="button" value="-" class="btn btn-circle btn-danger remove" />').end().appendTo("#form-container");
	});
	 
    $("#form-container").on("click", ".remove", function () {
     $(this).closest("div.form1").remove();

    });
	 $("#justify-icon").click(function() {
      
        $("#wrapper").toggleClass("toggled");
    });
	
});

 $("#start_date").datepicker();
$("#end_date").datepicker();

</script>

</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebApplication1
{
    public partial class CTop : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["New"] != null)
            {
                L1.Text = Session["New"].ToString();
            }
            else
                Response.Redirect("CompLogin.aspx");
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            con.Open();
            string Address = "Select Street from Company where Name='" + L1.Text + "'";
            string city = "Select City from Company where Name='" + L1.Text + "'";
            string State = "Select State from Company where Name='" + L1.Text + "'";
            string zip = "Select Zip from Company where Name='" + L1.Text + "'";
            string PhNum = "Select contact_number from Company where Name='" + L1.Text + "'";
            string website = "Select website from Company where Name='" + L1.Text + "'";
            SqlCommand Add = new SqlCommand(Address, con);
            SqlCommand c = new SqlCommand(city, con);
            SqlCommand s = new SqlCommand(State, con);
            SqlCommand z = new SqlCommand(zip, con);
            SqlCommand num = new SqlCommand(PhNum, con);
            SqlCommand web = new SqlCommand(website, con);
            string loc = Add.ExecuteScalar().ToString().Replace(" ", "");
            string city1 = c.ExecuteScalar().ToString().Replace(" ", "");
            string state1 = s.ExecuteScalar().ToString().Replace(" ", "");
            string zip1 = z.ExecuteScalar().ToString().Replace(" ", "");
            string phone = num.ExecuteScalar().ToString().Replace(" ", "");
            string link = web.ExecuteScalar().ToString().Replace(" ", "");
            LocLabel.Text = loc;
            CityLabel.Text = city1;
            StateLabel.Text = state1;
            ZIPLabel.Text = zip1;
            UrlLabel.Text = link;
            NumLabel.Text = phone;
            
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CView.aspx.cs" Inherits="WebApplication1.CView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="sidebar.css">
  <link rel="stylesheet" href="datepicker.css">
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="datepicker.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper" class="container-fluid ">

 <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#CTop.aspx"><h3><b>Company Portal<b></h3></a>
                </li>
				
                <li>
                    <a href="CPostJobs.aspx">Post Jobs</a>
                </li>
				
                <li>
                    <a href="CCurrentPosts.aspx">Current Postings</a>
                </li>
				
                <li>
                    <a href="https://docs.google.com/forms/d/1imbwezd520Qo7CHJ5SIYAfRCliFiHdp_9hIgfNhXMgU/viewform">Post Feedback</a>
                </li>
				
                
			<img src="mnsulogo.jpg" alt="mnsulogo" class="fix3" ></img>
			<li class="fix4">
                    <a href="CompLogin.aspx">Logout</a>
                </li>
        </div>
<div id = "form-container" class="container-fluid left">
<a href="#justify-icon" class="glyphicon glyphicon-align-justify size" id='justify-icon'></a>
</div>


<div class="fix2 container-fluid">

<div class="container-fluid left"  >
<h1 class="headclr">Company Portal </h1>
</div>
<div id="rcorners4">
<div class="container-fluid ">

<div class="two left">
<p><asp:Label ID="L1" runat="server" Text="Label" Font-Bold="True"></asp:Label></p>
<p><asp:Label ID="LocLabel" runat="server" Text="Label" Font-Size="X-Small"></asp:Label>
                               ,
                   <asp:Label ID="CityLabel" runat="server" Text="Label" Font-Size="X-Small"></asp:Label>
                               ,
                   <asp:Label ID="StateLabel" runat="server" Text="Label" Font-Size="X-Small"></asp:Label>
                               ,
                   <asp:Label ID="ZIPLabel" runat="server" Text="Label" Font-Size="X-Small"></asp:Label></p>
<p style="font-size:x-small">Ph Number:<asp:Label ID="NumLabel" runat="server" Text="Label" Font-Size="X-Small"></asp:Label></p>
<p style="font-size:x-small">Website:<asp:Label ID="UrlLabel" runat="server" Text="Label" Font-Size="X-Small"></asp:Label></p>
</div>

</div>
</div>

</div>



<br/>
    <div class="form-inline container-fluid center1 " >
        <div id="rcorners3"  class="container-fluid form-inline center2 ">
    <asp:ListView ID="ListView1" runat="server" >
                       <ItemTemplate>
                <table border="0" style="color:#520482">
                    <tr >
                        <td>
                            Internship ID:
                      </td>
                        <td>
                            <asp:Label ID="IP_ID" runat="server" Text='<%#Bind("Internship_Posting_ID") %>'></asp:Label>
                        </td>
                        </tr>      
                    <tr >
                        <td>
                            Job Title:
                      </td>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text='<%#Bind("Job_Title") %>'></asp:Label>
                        </td>
                        </tr>      
                    
                    <tr >
                        <td>
                            Company Name:
                      </td>
                        <td>
                            <asp:Label ID="Label2" runat="server" Text='<%#Bind("Name") %>'></asp:Label>
                        </td>
                        </tr>      
                    
                    <tr >
                        <td>
                            Location:
                      </td>
                        <td>
                            <asp:Label ID="Label3" runat="server" Text='<%#Bind("Street") %>'></asp:Label>, <asp:Label ID="Label11" runat="server" Text='<%#Bind("city") %>'></asp:Label>, <asp:Label ID="Label12" runat="server" Text='<%#Bind("state") %>'></asp:Label>- <asp:Label ID="Label13" runat="server" Text='<%#Bind("zip") %>'></asp:Label></td>
                        </td>
                        </tr>      
                    
                    <tr >
                        <td>
                            Desired Major:
                      </td>
                        <td>
                            <asp:Label ID="Label4" runat="server" Text='<%#Bind("Desired_Major") %>'></asp:Label>
                        </td>
                        </tr>      
                    
                    <tr >
                        <td>
                            Start Date:
                      </td>
                        <td>
                            <asp:Label ID="Label5" runat="server" Text='<%#Bind("Start_Date") %>'></asp:Label>
                        </td>
                        </tr>      
                    <tr >
                        <td>
                            End Date:
                      </td>
                        <td>
                            <asp:Label ID="Label6" runat="server" Text='<%#Bind("End_Date") %>'></asp:Label>
                        </td>
                        </tr>      
                    <tr >
                        <td>
                            Wages:
                      </td>
                        <td>
                            <asp:Label ID="Label7" runat="server" Text='<%#Bind("Wages") %>'></asp:Label>
                        </td>
                        </tr>      
                    <tr >
                        <td>
                            Hours/Week:
                      </td>
                        <td>
                            <asp:Label ID="Label8" runat="server" Text='<%#Bind("Hours") %>'></asp:Label>
                        </td>
                        </tr>      
                    
                    <tr >
                        <td>
                            Job Description:
                      </td>
                        <td>
                            <asp:Label ID="Label9" runat="server" Text='<%#Bind("Job_Description") %>'></asp:Label>
                        </td>
                        </tr>      
                    <tr >
                        <td>
                            Contact Person Name:
                      </td>
                        <td>
                            <asp:Label ID="Label10" runat="server" Text='<%#Bind("First_Name") %>'></asp:Label>
                        </td>
                        </tr> 
                    </table>
                           </ItemTemplate>
                   </asp:ListView>
         <p style="color:#520482">Job Qualifications:</p>
<asp:DataList ID="DataList2" runat="server">
                       <ItemTemplate>
                           <table class="auto-style1" style="color:#520482">
                               <tr>
                                   <td>
                                       <asp:Label ID="Label14" runat="server" Text='<%# Eval("Job_Qual_Name") %>'></asp:Label>
                                   </td>
                               </tr>
                               </table>
                       </ItemTemplate>
                   </asp:DataList>
 <br />
                <asp:LinkButton href="CCurrentPosts.aspx" ID="back" runat="server" style="color:#ff6a00">Back</asp:LinkButton>
 
 </div>
 <br/>
  
      
</div>
 
</form>
</body>
    <script>
$(function () {
    $("#btnAdd").click( function () {
    var cont = $("#add-form").clone();
	$(cont).find("#btnAdd").replaceWith('<input id="btnremove" type="button" value="-" class="btn btn-circle btn-danger remove" />').end().appendTo("#form-container");
	});
	 
    $("#form-container").on("click", ".remove", function () {
     $(this).closest("div.form1").remove();

    });
	 $("#justify-icon").click(function() {
      
        $("#wrapper").toggleClass("toggled");
    });
	
});

 $("#start_date").datepicker();
$("#end_date").datepicker();

</script>

</html>

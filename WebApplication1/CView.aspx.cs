﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace WebApplication1
{
    public partial class CView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["New"] != null)
            {
                L1.Text = Session["New"].ToString();
            }
            else
                Response.Redirect("CompLogin.aspx");
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            con.Open();
            string Address = "Select Street from Company where Name='" + L1.Text + "'";
            string city = "Select City from Company where Name='" + L1.Text + "'";
            string State = "Select State from Company where Name='" + L1.Text + "'";
            string zip = "Select Zip from Company where Name='" + L1.Text + "'";
            string PhNum = "Select contact_number from Company where Name='" + L1.Text + "'";
            string website = "Select website from Company where Name='" + L1.Text + "'";
            SqlCommand Add = new SqlCommand(Address, con);
            SqlCommand c = new SqlCommand(city, con);
            SqlCommand s = new SqlCommand(State, con);
            SqlCommand z = new SqlCommand(zip, con);
            SqlCommand num = new SqlCommand(PhNum, con);
            SqlCommand web = new SqlCommand(website, con);
            string loc = Add.ExecuteScalar().ToString().Replace(" ", "");
            string city1 = c.ExecuteScalar().ToString().Replace(" ", "");
            string state1 = s.ExecuteScalar().ToString().Replace(" ", "");
            string zip1 = z.ExecuteScalar().ToString().Replace(" ", "");
            string phone = num.ExecuteScalar().ToString().Replace(" ", "");
            string link = web.ExecuteScalar().ToString().Replace(" ", "");
            LocLabel.Text = loc;
            CityLabel.Text = city1;
            StateLabel.Text = state1;
            ZIPLabel.Text = zip1;
            UrlLabel.Text = link;
            NumLabel.Text = phone;

            if (!IsPostBack)
            {
                /*string ipid = "select  distinct ip.Internship_Posting_ID from Job j, Internship_Posting ip, Co_Contact_Person cp, Company c , Job_Qualification jq, Job_Qual_Intern jqi where j.Job_ID=ip.Job_ID and cp.Contact_Person_ID=ip.Contact_Person_ID and ip.Internship_Posting_ID=jqi.Internship_Posting_ID and jqi.Job_Qual_ID=jq.Job_Qual_ID and c.CompanyID=cp.CompanyID and c.Name='Titan';";
                SqlCommand da2 = new SqlCommand(ipid, con);
                string ip = da2.ExecuteScalar().ToString().Replace(" ", "");*/
                //string ip = Session["x"].ToString();
                SqlDataAdapter da = new SqlDataAdapter("select  distinct ip.Internship_Posting_ID, CAST(c.logo AS VARBINARY(MAX)) AS logo, j.Job_ID, j.Job_Title, ip.post_date, c.Name, c.Street, c.city, c.state, c.zip, ip.Desired_Major,ip.Wages,ip.Start_date, ip.End_Date, ip.Hours, j.Job_Description, cp.First_Name from Job j, Internship_Posting ip, Co_Contact_Person cp, Company c , Job_Qualification jq, Job_Qual_Intern jqi where j.Job_ID=ip.Job_ID and cp.Contact_Person_ID=ip.Contact_Person_ID and ip.Internship_Posting_ID=jqi.Internship_Posting_ID and jqi.Job_Qual_ID=jq.Job_Qual_ID and c.CompanyID=cp.CompanyID and ip.Internship_Posting_ID='" + Session["x"].ToString() + "';", con);
                DataSet ds = new DataSet();
                da.Fill(ds);
                ListView1.DataSource = ds.Tables[0];
                ListView1.DataBind();

            }
            if (!IsPostBack)
            {
                filldatalist();
            }
        }
        public void filldatalist()
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            con.Open();
            /*
            string ipid = "select  distinct ip.Internship_Posting_ID from Job j, Internship_Posting ip, Co_Contact_Person cp, Company c , Job_Qualification jq, Job_Qual_Intern jqi where j.Job_ID=ip.Job_ID and cp.Contact_Person_ID=ip.Contact_Person_ID and ip.Internship_Posting_ID=jqi.Internship_Posting_ID and jqi.Job_Qual_ID=jq.Job_Qual_ID and c.CompanyID=cp.CompanyID and c.Name='" + Session["New"].ToString() + "';";
            SqlCommand da2 = new SqlCommand(ipid, con);
            string ip = da2.ExecuteScalar().ToString().Replace(" ", "");
            */
            SqlDataAdapter jq = new SqlDataAdapter("select Job_Qual_name from Job_Qualification j, Job_Qual_Intern jqi, Internship_Posting ip where j.Job_Qual_ID=jqi.Job_Qual_ID and jqi.Internship_Posting_ID=ip.Internship_Posting_ID and ip.Internship_Posting_ID='" + Session["x"].ToString() + "';", con);
            DataSet ds = new DataSet();
            jq.Fill(ds);
            DataList2.DataSource = ds;
            DataList2.DataBind();
        }
    }
}
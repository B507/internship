﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebApplication1
{
    public partial class CheckStatus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["New"] != null)
            {
                L1.Text = Session["New"].ToString();
            }
            else
                Response.Redirect("StudLogin.aspx");
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            con.Open();
            string tid = "Select StudentID from Student where LTRIM(RTRIM(eFirstName+' '+eLastName))=LTRIM(RTRIM('" + L1.Text + "'))";
            string cid = "Select CollegeID from Student where LTRIM(RTRIM(eFirstName+' '+eLastName))=LTRIM(RTRIM('" + L1.Text + "'))";
            SqlCommand t = new SqlCommand(tid, con);
            SqlCommand c = new SqlCommand(cid, con);
            string tech = t.ExecuteScalar().ToString().Replace(" ", "");
            string clg = c.ExecuteScalar().ToString().Replace(" ", "");
            con.Close();
            L2.Text = tech;
            L3.Text = clg; if (!IsPostBack)
            {
                SqlDataAdapter da = new SqlDataAdapter("select distinct rs.appID, i.PostDate, e.AcademicTerm,J.Job_Title, c.Name, rs.Result from ResultStatus rs, company c, Job j, StudentAdvisor sa, InternshipForm i, Enrollment e where c.CompanyID=i.CompanyName and j.Job_ID=i.JobTitle and rs.StudentID='"+L2.Text+"' and rs.StudentID=sa.StudentID and i.StudentID=rs.StudentID and i.appID=rs.appID and e.StudentID=i.StudentID;", con);
                DataSet ds = new DataSet();
                da.Fill(ds);
                ListView1.DataSource = ds.Tables[0];
                ListView1.DataBind();

            }
        }
    }
}
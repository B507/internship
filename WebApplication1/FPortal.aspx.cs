﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebApplication1
{
    public partial class FPortal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["New"] != null)
            {
                L1.Text = Session["New"].ToString();
            }
            else
                Response.Redirect("FacultyLogin.aspx");
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            con.Open();
            string fid = "Select FacultyID from Faculty where LTRIM(RTRIM(FirstName+' '+LastName))=LTRIM(RTRIM('" + L1.Text + "'))";
            string cid = "Select CollegeID from Faculty where LTRIM(RTRIM(FirstName+' '+LastName))=LTRIM(RTRIM('" + L1.Text + "'))";
            SqlCommand f = new SqlCommand(fid, con);
            SqlCommand c = new SqlCommand(cid, con);
            string id = f.ExecuteScalar().ToString().Replace(" ", "");
            string clg = c.ExecuteScalar().ToString().Replace(" ", "");
            con.Close();
            L2.Text = id;
            L3.Text = clg;
        }
    }
}
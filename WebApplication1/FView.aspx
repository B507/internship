﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FView.aspx.cs" Inherits="WebApplication1.FView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="sidebar.css">
  <link rel="stylesheet" href="datepicker.css">
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="datepicker.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper" class="container-fluid ">

 <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="FPortal.aspx"><h3><b>Faculty Portal</b></h3></a>
                </li>
				
                <li>
                    <a href="FacultyReceivedApplications.aspx">Internship Applications</a>
                </li>
				
                
                
			<img src="mnsulogo.jpg" alt="mnsulogo" class="fix3" ></img>
			<li class="fix4">
                    <a href="FacultyLogin.aspx">Logout</a>
                </li>
        </div>
<div id = "form-container" class="container-fluid left">
<a href="#justify-icon" class="glyphicon glyphicon-align-justify size" id='justify-icon'></a>
</div>


<div class="fix2 container-fluid">

<div class="container-fluid left"  >
<h1 class="headclr">Faculty Portal </h1>
</div>
<div id="rcorners4">
<div class="container-fluid ">

<div class="two left">
<p><asp:Label ID="L1" runat="server" Text="Label" Font-Bold="True"></asp:Label></p>
<p style="font-size:x-small">FacultyID: <asp:Label ID="L2" runat="server" Text="Label"></asp:Label></p>
<p style="font-size:x-small">CollegeID: <asp:Label ID="L3" runat="server" Text="Label"></asp:Label></p>
</div>

</div>
</div>

</div>



<br/>
    <div class="form-inline container-fluid center1 " >
        <div id="rcorners3"  class="container-fluid form-inline center2 ">
        <asp:ListView ID="ListView1" runat="server" >
                       <ItemTemplate>
                <table border="0" style="color:#520482">
                    <tr >
                        <td>
                            Application ID:
                      </td>
                        <td>
                            <asp:Label ID="appID" runat="server" Text='<%#Bind("appID") %>'></asp:Label>
                        </td>
                        </tr>      
                    <tr >
                        <td>
                            Post Date:
                      </td>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text='<%#Bind("PostDate") %>'></asp:Label>
                        </td>
                        </tr>      
                    <tr>
                        <td>
                            <br />
                            <b>Student Details:</b>
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr >
                        <td>
                            Student ID:
                      </td>
                        <td>
                            <asp:Label ID="Label2" runat="server" Text='<%#Bind("StudentID") %>'></asp:Label>
                        </td>
                        </tr>      
                    
                    
                    <tr >
                        <td>
                            Student Name:
                      </td>
                        <td>
                            <asp:Label ID="Label4" runat="server" Text='<%#Bind("Name") %>'></asp:Label>
                        </td>
                        </tr>      
                    
                    <tr >
                        <td>
                            Student Email:
                      </td>
                        <td>
                            <asp:Label ID="Label5" runat="server" Text='<%#Bind("eEmail") %>'></asp:Label>
                        </td>
                        </tr>      
                    <tr >
                        <td>
                            <br />
                           <b> Company Details:</b>
                      </td>
                        <td>
                            
                        </td>
                        </tr>      
                    <tr >
                        <td>
                            Company Name:
                      </td>
                        <td>
                            <asp:Label ID="Label7" runat="server" Text='<%#Bind("cname") %>'></asp:Label>
                        </td>
                        </tr>      
                    <tr >
                        <td>
                            Company Address
                      </td>
                        <td>
                            <asp:Label ID="Label8" runat="server" Text='<%#Bind("CompanyStreetAddress") %>'></asp:Label>, <asp:Label ID="Label3" runat="server" Text='<%#Bind("State") %>'></asp:Label>, <asp:Label ID="Label6" runat="server" Text='<%#Bind("zip") %>'></asp:Label>
                        </td>
                        </tr>      
                    
                    <tr >
                        <td>
                           <br /><b> Job Details:</b>
                      </td>
                        <td>
                            
                        </td>
                        </tr>      
                    <tr >
                        <td>
                            Job Title:
                      </td>
                        <td>
                            <asp:Label ID="Label10" runat="server" Text='<%#Bind("Job_Title") %>'></asp:Label>
                        </td>
                        </tr> 
                    <tr >
                        <td>
                            Start Date:
                      </td>
                        <td>
                            <asp:Label ID="Label9" runat="server" Text='<%#Bind("StartDate") %>'></asp:Label>
                        </td>
                        </tr> 
                    <tr >
                        <td>
                            End Date:
                      </td>
                        <td>
                            <asp:Label ID="Label11" runat="server" Text='<%#Bind("EndDate") %>'></asp:Label>
                        </td>
                        </tr> 
                    <tr >
                        <td>
                            Hours/ WeekL
                      </td>
                        <td>
                            <asp:Label ID="Label12" runat="server" Text='<%#Bind("Hours") %>'></asp:Label>
                        </td>
                        </tr> 
                    <tr >
                        <td>
                            Salary:
                      </td>
                        <td>
                            <asp:Label ID="Label13" runat="server" Text='<%#Bind("wages") %>'></asp:Label>
                        </td>
                        </tr> 
                    <tr >
                        <td>
                            <br /><b>Supervisor Details:</b>
                      </td>
                        <td>
                            
                        </td>
                        </tr> <tr >
                        <td>
                            Supervisor Name
                      </td>
                        <td>
                            <asp:Label ID="Label16" runat="server" Text='<%#Bind("SupervisorName") %>'></asp:Label>
                        </td>
                        </tr> <tr >
                        <td>
                            Supervisor Email:
                      </td>
                        <td>
                            <asp:Label ID="Label17" runat="server" Text='<%#Bind("SupervisorEmail") %>'></asp:Label>
                        </td>
                        </tr> <tr >
                        <td>
                            Supervisor PhNumber:
                      </td>
                        <td>
                            <asp:Label ID="Label18" runat="server" Text='<%#Bind("SupervisorPhNum") %>'></asp:Label>
                        </td>
                        </tr> 

                    </table>
                           </ItemTemplate>
                   </asp:ListView>
        
        <br />
                
 
 
 <br/><p style="color:#520482"><b> Uploaded Files:</b></p>
  <asp:ListView ID="ListView2" runat="server" >
                       <ItemTemplate>
                <table border="0" style="color:#520482">
                   
                    <tr >
                        <td><asp:Label ID="Label19" runat="server" Text='<%#Bind("fname") %>'></asp:Label>
        </td>
                        <td>&nbsp;&nbsp;&nbsp;
                            <asp:LinkButton ID="lnkDownload" runat="server" Text="Download" OnClick="DownloadFile"
                    CommandArgument='<%# Eval("Id") %>'></asp:LinkButton>
                        </td>
                    </tr>
                    </table>
                           </ItemTemplate>
      </asp:ListView>
            <br />
            <asp:Button ID="AcceptBtn" runat="server" Text="Approve" class="btn purple" OnClick="AcceptBtn_Click"/><asp:Button ID="RejectBtn" runat="server" Text="Reject" class="btn purple right fix1" OnClick="RejectBtn_Click" />
            <br /><br /><asp:LinkButton href="FacultyReceivedApplications.aspx" ID="back" runat="server" style="color:#ff6a00">Back</asp:LinkButton>

            <br />
            <asp:Label ID="SuccessLabel" runat="server" Text="Success" ForeColor="Green" Visible="false"></asp:Label>
      </div>
</div>
 </form>
</body>
    <script>
$(function () {
    $("#btnAdd").click( function () {
    var cont = $("#add-form").clone();
	$(cont).find("#btnAdd").replaceWith('<input id="btnremove" type="button" value="-" class="btn btn-circle btn-danger remove" />').end().appendTo("#form-container");
	});
	 
    $("#form-container").on("click", ".remove", function () {
     $(this).closest("div.form1").remove();

    });
	 $("#justify-icon").click(function() {
      
        $("#wrapper").toggleClass("toggled");
    });
	
});

 $("#start_date").datepicker();
$("#end_date").datepicker();

</script>

</html>
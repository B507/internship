﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;
using System.Net;

namespace WebApplication1
{
    public partial class FView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["New"] != null)
            {
                L1.Text = Session["New"].ToString();
            }
            else
                Response.Redirect("FacultyLogin.aspx");
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            con.Open();
            string fid = "Select FacultyID from Faculty where LTRIM(RTRIM(FirstName+' '+LastName))=LTRIM(RTRIM('" + L1.Text + "'))";
            string cid = "Select CollegeID from Faculty where LTRIM(RTRIM(FirstName+' '+LastName))=LTRIM(RTRIM('" + L1.Text + "'))";
            SqlCommand f = new SqlCommand(fid, con);
            SqlCommand c = new SqlCommand(cid, con);
            string id = f.ExecuteScalar().ToString().Replace(" ", "");
            string clg = c.ExecuteScalar().ToString().Replace(" ", "");
            L2.Text = id;
            L3.Text = clg;
            
            if (!IsPostBack)
            {
                SqlDataAdapter da = new SqlDataAdapter("select distinct i.appID,   i.postdate, i.studentID, st.eFirstName+' '+st.eLastName as Name, st.eEmail, c.Name as cname, i.CompanyStreetAddress, i.State, i.ZIP, j.Job_Title, i.StartDate, i.EndDate, i.Hours, i.wages, cp.First_Name+' '+COALESCE(cp.Last_Name, '') as SupervisorName, i.SupervisorEMail, i.SupervisorPhNum from InternshipForm i, company c, student st, job j, studentAdvisor s, Co_Contact_Person cp where i.StudentID=s.StudentID and j.Job_ID=i.JobTitle and c.CompanyID=i.CompanyName and cp.CompanyID=i.CompanyName and i.StudentID=st.StudentID and s.FacultyID='" + L2.Text + "' and i.StudentID='" + Session["x"].ToString() + "' and i.appID=" + Session["a"].ToString() + ";", con);
                DataSet ds = new DataSet();
                da.Fill(ds);
                ListView1.DataSource = ds.Tables[0];
                ListView1.DataBind();

            }
            if (!IsPostBack)
            {
                SqlDataAdapter da = new SqlDataAdapter("select t.Id, t.Name as fname from tblFiles t, student st, studentAdvisor s where t.StudentID=s.StudentID and t.StudentID=st.StudentID and s.FacultyID='" + L2.Text + "' and t.StudentID='" + Session["x"].ToString() + "';", con);
                DataSet ds = new DataSet();
                da.Fill(ds);
                ListView2.DataSource = ds.Tables[0];
                ListView2.DataBind();

            }
            con.Close();
        }
        protected void DownloadFile(object sender, EventArgs e)
        {
            int id = int.Parse((sender as LinkButton).CommandArgument);
            byte[] bytes;
            string fileName, contentType;
            string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select Name, Data, ContentType from tblFiles where Id=@Id";
                    cmd.Parameters.AddWithValue("@Id", id);
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        sdr.Read();
                        bytes = (byte[])sdr["Data"];
                        contentType = sdr["ContentType"].ToString();
                        fileName = sdr["Name"].ToString();
                    }
                    con.Close();
                }
            }
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = contentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + fileName);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
        }

        protected void AcceptBtn_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            con.Open();
            SqlCommand com1 = new SqlCommand("select i.appID from InternshipForm i, student st, studentAdvisor s where i.StudentID=s.StudentID and i.StudentID=st.StudentID and s.FacultyID='" + L2.Text + "' and i.StudentID='" + Session["x"].ToString() + "';", con);
            int aid = Convert.ToInt32(com1.ExecuteScalar());
            SqlCommand com2 = new SqlCommand("select i.studentID from InternshipForm i, student st, studentAdvisor s where i.StudentID=s.StudentID and i.StudentID=st.StudentID and s.FacultyID='" + L2.Text + "' and i.StudentID='" + Session["x"].ToString() + "';", con);
            string sid = Convert.ToString(com2.ExecuteScalar());
           SqlCommand com = new SqlCommand("Insert into ResultStatus values(@appID, @studentID, @facultyID, @result)", con);
            com.Parameters.AddWithValue("@appID", aid);
            com.Parameters.AddWithValue("@studentID", sid);
            com.Parameters.AddWithValue("@facultyID", L2.Text);
            com.Parameters.AddWithValue("@result", "Approved");
            com.ExecuteNonQuery();

            
            string email = "Select Email from Faculty where LTRIM(RTRIM(FirstName+' '+LastName))=LTRIM(RTRIM('" + L1.Text + "'))";
            string pwd = "Select Password from Faculty where LTRIM(RTRIM(FirstName+' '+LastName))=LTRIM(RTRIM('" + L1.Text + "'))";
            SqlCommand frm = new SqlCommand(email, con);
            SqlCommand p = new SqlCommand(pwd, con);
            string fromEmail = frm.ExecuteScalar().ToString().Replace(" ", "");
            string fromPwd = p.ExecuteScalar().ToString().Replace(" ", "");
            foreach (ListViewItem item in ListView1.Items)
            {
                Label mylabel = (Label)item.FindControl("Label5");
                string toMail = mylabel.Text;
                Label app = (Label)item.FindControl("appID");
                string appID = app.Text; 
                Label PDate = (Label)item.FindControl("Label1");
                string PostDate = PDate.Text;
                Label CName = (Label)item.FindControl("Label7");
                string CompName = CName.Text;
                Label Job = (Label)item.FindControl("Label10");
                string Job_Title = Job.Text; 

            string subject = "Results of Your Internship Application:";
            string body = "Application ID: " + appID + "\n";
            body += "Applied Date: " + PostDate + "\n";
            body += "Company Name: " + CompName + "\n";
            body += "Job Title: " + Job_Title + "\n";
            body += "Has been approved....!!";

                var smtp = new System.Net.Mail.SmtpClient();
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(fromEmail, fromPwd);
                    smtp.Timeout = 20000;
                }
                smtp.Send(fromEmail, toMail, subject, body);
            }
            SuccessLabel.Text = "Approved";
            SuccessLabel.Visible = true;
        }

        protected void RejectBtn_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            con.Open();
            SqlCommand com1 = new SqlCommand("select i.appID from InternshipForm i, student st, studentAdvisor s where i.StudentID=s.StudentID and i.StudentID=st.StudentID and s.FacultyID='" + L2.Text + "' and i.StudentID='" + Session["x"].ToString() + "';", con);
            int aid = Convert.ToInt32(com1.ExecuteScalar());
            SqlCommand com2 = new SqlCommand("select i.studentID from InternshipForm i, student st, studentAdvisor s where i.StudentID=s.StudentID and i.StudentID=st.StudentID and s.FacultyID='" + L2.Text + "' and i.StudentID='" + Session["x"].ToString() + "';", con);
            string sid = Convert.ToString(com2.ExecuteScalar());
            SqlCommand com = new SqlCommand("Insert into ResultStatus values(@appID, @studentID, @facultyID, @result)", con);
            com.Parameters.AddWithValue("@appID", aid);
            com.Parameters.AddWithValue("@studentID", sid);
            com.Parameters.AddWithValue("@facultyID", L2.Text);
            com.Parameters.AddWithValue("@result", "Rejected");
            com.ExecuteNonQuery();

            string email = "Select Email from Faculty where LTRIM(RTRIM(FirstName+' '+LastName))=LTRIM(RTRIM('" + L1.Text + "'))";
            string pwd = "Select Password from Faculty where LTRIM(RTRIM(FirstName+' '+LastName))=LTRIM(RTRIM('" + L1.Text + "'))";
            SqlCommand frm = new SqlCommand(email, con);
            SqlCommand p = new SqlCommand(pwd, con);
            string fromEmail = frm.ExecuteScalar().ToString().Replace(" ", "");
            string fromPwd = p.ExecuteScalar().ToString().Replace(" ", "");
            foreach (ListViewItem item in ListView1.Items)
            {
                Label mylabel = (Label)item.FindControl("Label5");
                string toMail = mylabel.Text;
                Label app = (Label)item.FindControl("appID");
                string appID = app.Text;
                Label PDate = (Label)item.FindControl("Label1");
                string PostDate = PDate.Text;
                Label CName = (Label)item.FindControl("Label7");
                string CompName = CName.Text;
                Label Job = (Label)item.FindControl("Label10");
                string Job_Title = Job.Text;

                string subject = "Results of Your Internship Application:";
                string body = "Application ID: " + appID + "\n";
                body += "Applied Date: " + PostDate + "\n";
                body += "Company Name: " + CompName + "\n";
                body += "Job Title: " + Job_Title + "\n";
                body += "Has been rejected....!!";

                var smtp = new System.Net.Mail.SmtpClient();
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(fromEmail, fromPwd);
                    smtp.Timeout = 20000;
                }
                smtp.Send(fromEmail, toMail, subject, body);
            }
            SuccessLabel.Text = "Rejected";
            
            SuccessLabel.Visible = true;
        }

    }
}
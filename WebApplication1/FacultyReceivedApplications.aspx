﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FacultyReceivedApplications.aspx.cs" Inherits="WebApplication1.FInterApplication" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="sidebar.css">
  <link rel="stylesheet" href="datepicker.css">
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="datepicker.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper" class="container-fluid ">

 <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="FPortal.aspx"><h3><b>Faculty Portal</b></h3></a>
                </li>
				
                <li>
                    <a href="FacultyReceivedApplications.aspx">Internship Applications</a>
                </li>
				
                
                
			<img src="mnsulogo.jpg" alt="mnsulogo" class="fix3" ></img>
			<li class="fix4">
                    <a href="FacultyLogin.aspx">Logout</a>
                </li>
        </div>
<div id = "form-container" class="container-fluid left">
<a href="#justify-icon" class="glyphicon glyphicon-align-justify size" id='justify-icon'></a>
</div>


<div class="fix2 container-fluid">

<div class="container-fluid left"  >
<h1 class="headclr">Faculty Portal </h1>
</div>
<div id="rcorners4">
<div class="container-fluid ">

<div class="two left">
<p><asp:Label ID="L1" runat="server" Text="Label" Font-Bold="True"></asp:Label></p>
<p style="font-size:x-small">FacultyID: <asp:Label ID="L2" runat="server" Text="Label"></asp:Label></p>
<p style="font-size:x-small">CollegeID: <asp:Label ID="L3" runat="server" Text="Label"></asp:Label></p>
</div>

</div>
</div>

</div>

<br/>
        <asp:ListView ID="ListView2" runat="server" >
                
            <ItemTemplate>
                <table border="0">
                    <tr ><td>
                        <div id="rcorners3"  class="container-fluid form-inline center2 ">
                        <div class="left">
  
  </div><div id="rcorners1">
<div class="left">
<b><p><asp:Label ID="Labelx" runat="server" Text='<%#Bind("StudentID") %>' Font-Bold="True" Font-Size="Medium" ForeColor="#520482"></asp:Label><asp:Label ID="Label7" runat="server" Text='<%#Bind("appID") %>' Visible="false"></asp:Label></p>
</div>
<div class="right">
<p><asp:Label ID="Label6" runat="server" Text='<%#Bind("PostDate") %>'></asp:Label><p>
    <asp:Button ID="ViewButton" runat="server" Text="View" OnClick="getView" class="btn purple"/></div>
</div></div>
                  </td>  </tr>
                    </table>
                <br />
                </ItemTemplate>
            </asp:ListView>
        
   

    


 
 </div>
 <br/>
  
 </form>
</body>
    <script>
$(function () {
    $("#btnAdd").click( function () {
    var cont = $("#add-form").clone();
	$(cont).find("#btnAdd").replaceWith('<input id="btnremove" type="button" value="-" class="btn btn-circle btn-danger remove" />').end().appendTo("#form-container");
	});
	 
    $("#form-container").on("click", ".remove", function () {
     $(this).closest("div.form1").remove();

    });
	 $("#justify-icon").click(function() {
      
        $("#wrapper").toggleClass("toggled");
    });
	
});

 $("#start_date").datepicker();
$("#end_date").datepicker();

</script>

</html>
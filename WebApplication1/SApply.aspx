﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SApply.aspx.cs" Inherits="WebApplication1.SApply" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="sidebar.css">
  <link rel="stylesheet" href="datepicker.css">
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="datepicker.js"></script>
</head>
<body>
    <form id="form1" runat="server">
<div id="wrapper" class="container-fluid ">

 <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="STop.aspx"><h3><b>Student Portal</b></h3></a>
                </li>
				
                <li>
                    <a href="SApply.aspx">Apply</a>
                </li>
				
                <li>
                    <a href="SCurrentPosts.aspx">Current Postings</a>
                </li>
				
                <li>
                    <a href="https://docs.google.com/forms/d/1imbwezd520Qo7CHJ5SIYAfRCliFiHdp_9hIgfNhXMgU/viewform">Post Feedback</a>
                </li>
				<li>
                    <a href="CheckStatus.aspx">Check Status</a>
                </li>
                
			<img src="mnsulogo.jpg" alt="mnsulogo" class="fix3" ></img>
			<li class="fix4">
                    <a href="StudLogin.aspx">Logout</a>
                </li>
        </div>
<div id = "form-container" class="container-fluid left">
<a href="#justify-icon" class="glyphicon glyphicon-align-justify size" id='justify-icon'></a>
</div>


<div class="fix2 container-fluid">

<div class="container-fluid left"  >
<h1 class="headclr">Apply for Internship</h1>
</div>
<div id="rcorners4">
<div class="container-fluid ">

<div class="two left">
<p><asp:Label ID="L1" runat="server" Text="Label" Font-Bold="True"></asp:Label></p>
<p style="font-size:x-small">TechID: <asp:Label ID="L2" runat="server" Text="Label"></asp:Label></p>
<p style="font-size:x-small">CollegeID: <asp:Label ID="L3" runat="server" Text="Label"></asp:Label></p>
</div>

</div>
</div>

</div>



<br/>
    
 <br/>
  <div id="rcorners3"  class="container-fluid form-inline center2 ">
      <table border="0" style="color:#520482">
                            <tr>
                                <td rowspan="6" ><center style="font-weight: 700">Employer Information</center></td>
                                <td>Company</td>
                                <td colspan="3"><asp:DropDownList ID="DDL_Company" class="form-control" Width="250px" OnSelectedIndexChanged="DDL_Company_SelectedIndexChanged" runat="server" ViewStateMode="Enabled" EnableViewState="true" AutoPostBack="true"></asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td>
                                    Supervisor Name
                                </td><td colspan="3"><asp:DropDownList ID="DDL_Contact" class="form-control" Width="250px" OnSelectedIndexChanged="DDL_Contact_SelectedIndexChanged" runat="server" ViewStateMode="Enabled" EnableViewState="true" AutoPostBack="true"></asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td>
                                    Street Address
                                </td><td colspan="3"><asp:TextBox ID="Address" class="form-control" runat="server"/></td>
                            </tr>
                            <tr>
                                <td>
                                    State, ZIP
                                </td><td colspan="3"><asp:TextBox ID="State" class="form-control" runat="server"/><asp:TextBox ID="zip" class="form-control" runat="server"/></td>
                            </tr>
                            <tr>
                                <td>
                                    Supervisor E-mail
                                </td><td colspan="3"><asp:TextBox ID="SEMail" class="form-control" runat="server"/></td>
                            </tr>
                            <tr>
                                <td>
                                    Supervisor Phone
                                </td><td colspan="3"><asp:TextBox ID="SPhNum" class="form-control" runat="server"/></td>
                            </tr>
                            <tr>
                                <td rowspan="3"><center style="font-weight: 700">Job Information</center></td>
                                <td>Job Title</td>
                                <td colspan="3"><asp:DropDownList ID="DDL_Job" class="form-control" Width="250px" OnSelectedIndexChanged="DDL_Job_SelectedIndexChanged" runat="server" ViewStateMode="Enabled" EnableViewState="true" AutoPostBack="true"></asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td>
                                    Start Date
                                </td><td><asp:TextBox ID="Start_Date" class="form-control" type="text" runat="server"/></td>
                                <td>
                                    &nbsp;&nbsp;&nbsp;&nbsp;End Date
                                </td><td><asp:TextBox ID="End_Date" class="form-control" type="text" runat="server"/></td>
                            </tr>
                            <tr>
                                <td>
                                    Hours/Week
                                </td><td><asp:TextBox ID="Hours" class="form-control" Width="150px" runat="server"/>hr/wk</td>
                                <td>
                                    &nbsp;&nbsp;&nbsp;&nbsp;Salary/Hour
                                </td><td><asp:TextBox ID="Wage" class="form-control" Width="150px" runat="server"/>$/hr</td>
                            </tr>
                            
                        </table>

</div><br />
    <div class="form-inline container-fluid center1 " >
<asp:Button ID="Apply_Button" runat="server" class="btn purple" style="margin-left: 0px" Text="Next" OnClick="Apply_Button_Click" />
    
        
 
 </div>
 
    </form>
</body>

    <script>
$(function () {
    $("#btnAdd").click( function () {
    var cont = $("#add-form").clone();
	$(cont).find("#btnAdd").replaceWith('<input id="btnremove" type="button" value="-" class="btn btn-circle btn-danger remove" />').end().appendTo("#form-container");
	});
	 
    $("#form-container").on("click", ".remove", function () {
     $(this).closest("div.form1").remove();

    });
	 $("#justify-icon").click(function() {
      
        $("#wrapper").toggleClass("toggled");
    });
	
});

 $("#start_date").datepicker();
$("#end_date").datepicker();

</script>

</html>

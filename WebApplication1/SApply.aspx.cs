﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Configuration;

namespace WebApplication1
{
    public partial class SApply : System.Web.UI.Page
    {
            
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["New"] != null)
            {
                L1.Text = Session["New"].ToString();
            }
            else
                Response.Redirect("StudLogin.aspx");
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            con.Open();
        
            string tid = "Select StudentID from Student where LTRIM(RTRIM(eFirstName+' '+eLastName))=LTRIM(RTRIM('" + L1.Text + "'))";
            string cid = "Select CollegeID from Student where LTRIM(RTRIM(eFirstName+' '+eLastName))=LTRIM(RTRIM('" + L1.Text + "'))";
            SqlCommand t = new SqlCommand(tid, con);
            SqlCommand c = new SqlCommand(cid, con);
            string tech = t.ExecuteScalar().ToString().Replace(" ", "");
            string clg = c.ExecuteScalar().ToString().Replace(" ", "");
            con.Close();
            L2.Text = tech;
            L3.Text = clg;

            if (!IsPostBack)
            {
                SqlCommand dd = new SqlCommand("select distinct CompanyID, Name from Company;", con);
                SqlDataAdapter da = new SqlDataAdapter(dd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                DDL_Company.DataSource = ds;
                DDL_Company.DataTextField = "Name";
                DDL_Company.DataValueField = "CompanyID";
                DDL_Company.DataBind();
                DDL_Company.Items.Insert(0, new ListItem("---Company Name---", "0"));
            }
        }

        protected void DDL_Company_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            con.Open();

            SqlCommand dd2 = new SqlCommand("select cp.First_Name+' '+COALESCE(cp.Last_Name, '') as Name from Co_Contact_Person cp, company c where cp.CompanyID=c.CompanyID and c.CompanyID='" + DDL_Company.SelectedValue + "';", con);
                SqlDataAdapter da2 = new SqlDataAdapter(dd2);
                DataSet ds2 = new DataSet();
                da2.Fill(ds2);
                DDL_Contact.DataSource = ds2;
                DDL_Contact.DataTextField = "Name";
                DDL_Contact.DataValueField = "Name";
                DDL_Contact.DataBind();
                DDL_Contact.Items.Insert(0, new ListItem("---Contact Person---", "0"));
           
                SqlCommand dd3 = new SqlCommand("select j.Job_ID, j.Job_Title from Internship_Posting i, Job j, Company c, Co_Contact_Person cp where j.Job_ID=i.Job_ID and i.Contact_Person_ID=cp.Contact_Person_ID and cp.CompanyID=c.CompanyID and c.CompanyID='" + DDL_Company.SelectedValue + "';", con);
                SqlDataAdapter da3 = new SqlDataAdapter(dd3);
                DataSet ds3 = new DataSet();
                da3.Fill(ds3);
                DDL_Job.DataSource = ds3;
                DDL_Job.DataTextField = "Job_Title";
                DDL_Job.DataValueField = "Job_ID";
                DDL_Job.DataBind();
                DDL_Job.Items.Insert(0, new ListItem("---Select Job---", "0"));
            
            SqlCommand com_add = new SqlCommand("select Street+', '+city from company where CompanyID='"+DDL_Company.SelectedValue+"'", con);
            Address.Text = Convert.ToString(com_add.ExecuteScalar());
            SqlCommand com_state = new SqlCommand("select State from company where CompanyID='" + DDL_Company.SelectedValue + "'", con);
            State.Text = Convert.ToString(com_state.ExecuteScalar());
            SqlCommand com_zip = new SqlCommand("select ZIP from company where CompanyID='" + DDL_Company.SelectedValue + "'", con);
            zip.Text = Convert.ToString(com_zip.ExecuteScalar());
            
        }
        protected void DDL_Contact_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            con.Open();
            SqlCommand semail = new SqlCommand("select mailID from Co_Contact_Person where CompanyID='" + DDL_Company.SelectedValue + "' and First_Name+' '+COALESCE(Last_Name, '')='" + DDL_Contact.SelectedValue + "'", con);
            SEMail.Text = Convert.ToString(semail.ExecuteScalar());
            SqlCommand sph = new SqlCommand("select contact_number from Co_Contact_Person where CompanyID='" + DDL_Company.SelectedValue + "' and First_Name+' '+COALESCE(Last_Name, '')='" + DDL_Contact.SelectedValue + "'", con);
            SPhNum.Text = Convert.ToString(sph.ExecuteScalar());
            
        }
        protected void DDL_Job_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            con.Open();
            SqlCommand start = new SqlCommand("select start_date from Internship_Posting  where job_id='" + DDL_Job.SelectedValue + "'", con);
            Start_Date.Text = Convert.ToString(start.ExecuteScalar());
            SqlCommand end = new SqlCommand("select end_date from Internship_Posting  where job_id='" + DDL_Job.SelectedValue + "'", con);
            End_Date.Text = Convert.ToString(end.ExecuteScalar());
            SqlCommand h = new SqlCommand("select Hours from Internship_Posting  where job_id='" + DDL_Job.SelectedValue + "'", con);
            Hours.Text = Convert.ToString(h.ExecuteScalar());
            SqlCommand w = new SqlCommand("select Wages from Internship_Posting  where job_id='" + DDL_Job.SelectedValue + "'", con);
            Wage.Text = Convert.ToString(w.ExecuteScalar());
            
        }
        protected void Apply_Button_Click(object sender, EventArgs e)
        {
            SqlConnection vid = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            {
                try
                {
                    SqlCommand com1;
                    vid.Open();
                    SqlCommand d = new SqlCommand("select getdate()", vid);
                    DateTime dt = Convert.ToDateTime(d.ExecuteScalar());
                    DateTime sdt = Convert.ToDateTime(Start_Date.Text);
                    DateTime edt = Convert.ToDateTime(End_Date.Text);
                    /*DateTime sdate = Convert.ToDateTime(Start_Date);
                    DateTime edate = Convert.ToDateTime(End_Date);*/
                    com1 = new SqlCommand("insert into InternshipForm (StudentID, CompanyName, SupervisorName, CompanyStreetAddress, State, ZIP, SupervisorEMail, SupervisorPhNum, JobTitle, StartDate, EndDate, Hours, wages, PostDate) values(@StudentID, @CompanyName, @SupervisorName, @CompanyStreetAddress, @State, @ZIP, @SupervisorEMail, @SupervisorPhNum, @JobTitle, @StartDate, @EndDate, @Hours, @wages, '" + dt + "')", vid);
                    com1.Parameters.AddWithValue("@StudentID", L2.Text);
                    com1.Parameters.AddWithValue("@CompanyName", DDL_Company.Text);
                    com1.Parameters.AddWithValue("@SupervisorName", DDL_Contact.Text);
                    com1.Parameters.AddWithValue("@CompanyStreetAddress", Address.Text);
                    com1.Parameters.AddWithValue("@State", State.Text);
                    com1.Parameters.AddWithValue("@ZIP", zip.Text);
                    com1.Parameters.AddWithValue("@SupervisorEMail", SEMail.Text);
                    com1.Parameters.AddWithValue("@SupervisorPhNum", SPhNum.Text);
                    com1.Parameters.AddWithValue("@JobTitle", DDL_Job.Text);
                    com1.Parameters.AddWithValue("@StartDate", Start_Date.Text);
                    com1.Parameters.AddWithValue("@EndDate", End_Date.Text);
                    com1.Parameters.AddWithValue("@Hours", Hours.Text);
                    com1.Parameters.AddWithValue("@wages", Wage.Text);
                    com1.ExecuteNonQuery();

                    vid.Close();
                    Response.Redirect("SApplyUpload.aspx");
                }
                catch (Exception ex)
                {
                    Response.Write("Error:" + ex.ToString());
                }
            }
        }
        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebApplication1
{
    public partial class SApplyUpload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           if (Session["New"] != null)
            {
                L1.Text = Session["New"].ToString();
            }
            else
                Response.Redirect("StudLogin.aspx");
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            con.Open();
            string tid = "Select StudentID from Student where LTRIM(RTRIM(eFirstName+' '+eLastName))=LTRIM(RTRIM('" + L1.Text + "'))";
            string cid = "Select CollegeID from Student where LTRIM(RTRIM(eFirstName+' '+eLastName))=LTRIM(RTRIM('" + L1.Text + "'))";
            SqlCommand t = new SqlCommand(tid, con);
            SqlCommand c = new SqlCommand(cid, con);
            string tech = t.ExecuteScalar().ToString().Replace(" ", "");
            string clg = c.ExecuteScalar().ToString().Replace(" ", "");
            con.Close();
            L2.Text = tech;
            L3.Text = clg;
        }
        protected void Upload(object sender, EventArgs e)
        {
            string filename = Path.GetFileName(FileUpload1.PostedFile.FileName);
            string contentType = FileUpload1.PostedFile.ContentType;
            using (Stream fs = FileUpload1.PostedFile.InputStream)
            {
                using (BinaryReader br = new BinaryReader(fs))
                {
                    byte[] bytes = br.ReadBytes((Int32)fs.Length);
                    string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    using (SqlConnection con = new SqlConnection(constr))
                    {
                        
                        string query = "insert into tblFiles values (@Name, @ContentType, @Data, @StudentID)";
                        using (SqlCommand cmd = new SqlCommand(query))
                        {
                            cmd.Connection = con;
                            cmd.Parameters.AddWithValue("@Name", filename);
                            cmd.Parameters.AddWithValue("@ContentType", contentType);
                            cmd.Parameters.AddWithValue("@Data", bytes);
                            cmd.Parameters.AddWithValue("@StudentID", L2.Text);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                            lblSuccess.Visible = true;
                        }

                    }
                }
            }
           // Response.Redirect(Request.Url.AbsoluteUri);
        }
 
    }
}
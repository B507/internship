﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SCurrentPosts.aspx.cs" Inherits="WebApplication1.SCurrentPosts" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="sidebar.css">
  <link rel="stylesheet" href="datepicker.css">
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="datepicker.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper" class="container-fluid ">

 <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="STop.aspx"><h3><b>Student Portal<b></h3></a>
                </li>
				
                <li>
                    <a href="SApply.aspx">Apply</a>
                </li>
				
                <li>
                    <a href="SCurrentPosts.aspx">Current Postings</a>
                </li>
				
                <li>
                    <a href="https://docs.google.com/forms/d/1imbwezd520Qo7CHJ5SIYAfRCliFiHdp_9hIgfNhXMgU/viewform">Post Feedback</a>
                </li>
				<li>
                    <a href="CheckStatus.aspx">Check Status</a>
                </li>
                
			<img src="mnsulogo.jpg" alt="mnsulogo" class="fix3" ></img>
			<li class="fix4">
                    <a href="studLogin.aspx">Logout</a>
                </li>
        </div>
<div id = "form-container" class="container-fluid left">
<a href="#justify-icon" class="glyphicon glyphicon-align-justify size" id='justify-icon'></a>
</div>


<div class="fix2 container-fluid">

<div class="container-fluid left"  >
<h1 class="headclr">Current Postings </h1>
</div>
<div id="rcorners4">
<div class="container-fluid ">

<div class="two left">
<p><asp:Label ID="L1" runat="server" Text="Label" Font-Bold="True"></asp:Label></p>
<p style="font-size:x-small">TechID: <asp:Label ID="L2" runat="server" Text="Label"></asp:Label></p>
<p style="font-size:x-small">CollegeID: <asp:Label ID="L3" runat="server" Text="Label"></asp:Label></p>
</div>

</div>
</div>

</div>



<br/>
    <div class="form-inline container-fluid center1 " >
        


 <select id="subject" class="form-control right fix1">
    <option value="" disabled selected>Filter</option>
    <option value="EE">EE</option>
    <option value="IT">IT</option>
	<option value="ME">ME</option>
  </select>	
 </div>
 <br/>

        
            <asp:ListView ID="ListView2" runat="server" >
                
            <ItemTemplate>
                <table border="0">
                    <tr ><td>
                        <div id="rcorners3"  class="container-fluid form-inline center2 ">
                        <div class="left">
  <img src="<%#Eval("logo")%>" alt="logo" style="height:80px"></img></a>
  </div><div id="rcorners1">
<div class="left">
<b><p><asp:Label ID="Labelx" runat="server" Text='<%#Bind("Job_Title") %>' Font-Bold="True" Font-Size="Medium" ForeColor="#520482"></asp:Label></p>
<p><asp:Label ID="Label1" runat="server" Text='<%#Bind("Name") %>'></asp:Label><asp:Label ID="Label7" runat="server" Text='<%#Bind("Internship_Posting_ID") %>' Visible="false"></asp:Label></p></b>
<p style="font-size:x-small"> <asp:Label ID="Label2" runat="server" Text='<%#Bind("Street") %>'></asp:Label>, <asp:Label ID="Label3" runat="server" Text='<%#Bind("city") %>'></asp:Label>, <asp:Label ID="Label4" runat="server" Text='<%#Bind("state") %>'></asp:Label>- <asp:Label ID="Label5" runat="server" Text='<%#Bind("zip") %>'></asp:Label></p>
</div>
<div class="right">
<p><asp:Label ID="Label6" runat="server" Text='<%#Bind("post_date") %>'></asp:Label><p>
    <asp:Button ID="ViewButton" runat="server" Text="View" class="btn purple" OnClick="getView"/></div>
</div></div>
                  </td>  </tr>
                    </table>
                <br />
                </ItemTemplate>
            </asp:ListView>
        
 <br/>
   
        </div>
 </form>
</body>
    <script>
$(function () {
    $("#btnAdd").click( function () {
    var cont = $("#add-form").clone();
	$(cont).find("#btnAdd").replaceWith('<input id="btnremove" type="button" value="-" class="btn btn-circle btn-danger remove" />').end().appendTo("#form-container");
	});
	 
    $("#form-container").on("click", ".remove", function () {
     $(this).closest("div.form1").remove();

    });
	 $("#justify-icon").click(function() {
      
        $("#wrapper").toggleClass("toggled");
    });
	
});

 $("#start_date").datepicker();
$("#end_date").datepicker();

</script>
</html>

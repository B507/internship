﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebApplication1
{
    public partial class STop : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["New"] != null)
            {
                L1.Text = Session["New"].ToString();
            }
            else
                Response.Redirect("StudLogin.aspx");
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            con.Open();
            string tid = "Select StudentID from Student where LTRIM(RTRIM(eFirstName+' '+eLastName))=LTRIM(RTRIM('" + L1.Text + "'))";
            string cid = "Select CollegeID from Student where LTRIM(RTRIM(eFirstName+' '+eLastName))=LTRIM(RTRIM('" + L1.Text + "'))";
            SqlCommand t = new SqlCommand(tid, con);
            SqlCommand c = new SqlCommand(cid, con);
            string tech = t.ExecuteScalar().ToString().Replace(" ", "");
            string clg = c.ExecuteScalar().ToString().Replace(" ", "");
            con.Close();
            L2.Text = tech;
            L3.Text = clg;
        }
    }
}
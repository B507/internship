﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebApplication1
{
    public partial class SView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["New"] != null)
            {
                L1.Text = Session["New"].ToString();
            }
            else
                Response.Redirect("StudLogin.aspx");
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            con.Open();
            string tid = "Select StudentID from Student where LTRIM(RTRIM(eFirstName+' '+eLastName))=LTRIM(RTRIM('" + L1.Text + "'))";
            string cid = "Select CollegeID from Student where LTRIM(RTRIM(eFirstName+' '+eLastName))=LTRIM(RTRIM('" + L1.Text + "'))";
            SqlCommand t = new SqlCommand(tid, con);
            SqlCommand c = new SqlCommand(cid, con);
            string tech = t.ExecuteScalar().ToString().Replace(" ", "");
            string clg = c.ExecuteScalar().ToString().Replace(" ", "");
            L2.Text = tech;
            L3.Text = clg;

            if (!IsPostBack)
            {
                SqlDataAdapter da = new SqlDataAdapter("select  distinct ip.Internship_Posting_ID, CAST(c.logo AS VARBINARY(MAX)) AS logo, j.Job_ID, j.Job_Title, ip.post_date, c.Name, c.Street, c.city, c.state, c.zip, ip.Desired_Major,ip.Wages,ip.Start_date, ip.End_Date, ip.Hours, j.Job_Description, cp.First_Name from Job j, Internship_Posting ip, Co_Contact_Person cp, Company c , Job_Qualification jq, Job_Qual_Intern jqi where j.Job_ID=ip.Job_ID and cp.Contact_Person_ID=ip.Contact_Person_ID and ip.Internship_Posting_ID=jqi.Internship_Posting_ID and jqi.Job_Qual_ID=jq.Job_Qual_ID and c.CompanyID=cp.CompanyID and ip.Internship_Posting_ID='" + Session["x"].ToString() + "';", con);
                DataSet ds = new DataSet();
                da.Fill(ds);
                ListView1.DataSource = ds.Tables[0];
                ListView1.DataBind();

            }
            if (!IsPostBack)
            {
                filldatalist();
            }
        }
        public void filldatalist()
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            con.Open();
            /*
            string ipid = "select  distinct ip.Internship_Posting_ID from Job j, Internship_Posting ip, Co_Contact_Person cp, Company c , Job_Qualification jq, Job_Qual_Intern jqi where j.Job_ID=ip.Job_ID and cp.Contact_Person_ID=ip.Contact_Person_ID and ip.Internship_Posting_ID=jqi.Internship_Posting_ID and jqi.Job_Qual_ID=jq.Job_Qual_ID and c.CompanyID=cp.CompanyID and c.Name='" + Session["New"].ToString() + "';";
            SqlCommand da2 = new SqlCommand(ipid, con);
            string ip = da2.ExecuteScalar().ToString().Replace(" ", "");
            */
            SqlDataAdapter jq = new SqlDataAdapter("select Job_Qual_name from Job_Qualification j, Job_Qual_Intern jqi, Internship_Posting ip where j.Job_Qual_ID=jqi.Job_Qual_ID and jqi.Internship_Posting_ID=ip.Internship_Posting_ID and ip.Internship_Posting_ID='" + Session["x"].ToString() + "';", con);
            DataSet ds = new DataSet();
            jq.Fill(ds);
            DataList2.DataSource = ds;
            DataList2.DataBind();
        }

        protected void ApplyBtn_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            con.Open();
            SqlCommand w = new SqlCommand("select  distinct c.website from Job j, Internship_Posting ip, Co_Contact_Person cp, Company c , Job_Qualification jq, Job_Qual_Intern jqi where j.Job_ID=ip.Job_ID and cp.Contact_Person_ID=ip.Contact_Person_ID and ip.Internship_Posting_ID=jqi.Internship_Posting_ID and jqi.Job_Qual_ID=jq.Job_Qual_ID and c.CompanyID=cp.CompanyID and ip.Internship_Posting_ID='" + Session["x"].ToString() + "';", con);
            string url = w.ExecuteScalar().ToString().Replace(" ", ""); 
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('" + url + "');", true);
        }
    }
}